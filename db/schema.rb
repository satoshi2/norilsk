# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170822142322) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.integer "resource_id"
    t.string "author_type"
    t.integer "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "ad_groups", force: :cascade do |t|
    t.string "name"
    t.integer "parent_id", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "state", default: 0
    t.integer "ad_kind_id"
    t.integer "paid", default: 0
  end

  create_table "ad_kinds", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "ads", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.string "price"
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "published_until"
    t.string "status", default: "active"
    t.integer "hide"
    t.integer "ad_group_id"
    t.integer "state"
    t.integer "count"
    t.string "name"
    t.integer "user_id"
    t.integer "ad_kind_id"
    t.text "sellout"
  end

  create_table "albums", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.string "address"
    t.string "email"
    t.string "social"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "count"
    t.text "sellout"
  end

  create_table "areas", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "file_name"
    t.integer "file_size"
    t.string "file_status", default: "new"
    t.string "file_location"
    t.string "content_type"
  end

  create_table "atm_group_links", force: :cascade do |t|
    t.integer "atm_id"
    t.integer "atm_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "atm_groups", force: :cascade do |t|
    t.string "name"
    t.integer "parent_id", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "atm_kinds", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "atms", force: :cascade do |t|
    t.string "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "atm_kind_id"
    t.float "latitude"
    t.float "longtitude"
    t.integer "area_id"
  end

  create_table "baners", force: :cascade do |t|
    t.string "title"
    t.string "body"
    t.integer "banerable_id"
    t.string "banerable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "file_name"
    t.integer "file_size"
    t.string "file_status", default: "new"
    t.string "file_location"
    t.string "content_type"
  end

  create_table "banner_cinemas", force: :cascade do |t|
    t.string "title"
    t.integer "movie_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cinema_baners", force: :cascade do |t|
    t.string "name"
    t.integer "movie_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cinemas", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.string "address"
    t.text "description"
    t.string "email"
    t.string "report_email"
    t.string "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "rating"
    t.text "work_time"
    t.string "social"
    t.text "sellout"
  end

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.string "address"
    t.string "url"
    t.text "description"
    t.string "email"
    t.float "rating"
    t.string "social"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "report_email"
    t.text "work_time"
    t.text "sellout"
    t.integer "area_id"
  end

  create_table "company_group_links", force: :cascade do |t|
    t.integer "company_group_id"
    t.integer "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "company_groups", force: :cascade do |t|
    t.string "name"
    t.integer "parent_id", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "file_name"
    t.integer "file_size"
    t.string "file_status", default: "new"
    t.string "file_location"
    t.string "content_type"
  end

  create_table "covers", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "file_name"
    t.integer "file_size"
    t.string "file_status", default: "new"
    t.string "file_location"
    t.string "content_type"
    t.string "kind"
  end

  create_table "deliveries", force: :cascade do |t|
    t.integer "user_id"
    t.integer "house_id"
    t.integer "porch"
    t.string "code"
    t.integer "floor"
    t.integer "apartment"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dictionaries", force: :cascade do |t|
    t.string "kind"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "emergencies", force: :cascade do |t|
    t.string "title"
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "event_group_links", force: :cascade do |t|
    t.integer "event_id"
    t.integer "event_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "event_groups", force: :cascade do |t|
    t.string "name"
    t.integer "parent_id", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", force: :cascade do |t|
    t.string "name"
    t.datetime "begin_at"
    t.datetime "finish_at"
    t.text "description"
    t.string "phone"
    t.string "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "counter"
    t.text "sellout"
    t.integer "area_id"
  end

  create_table "exchanges", force: :cascade do |t|
    t.string "name"
    t.string "source_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "extra_resources", force: :cascade do |t|
    t.string "kind"
    t.string "source_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "food_group_links", force: :cascade do |t|
    t.integer "food_id"
    t.integer "food_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "food_groups", force: :cascade do |t|
    t.string "name"
    t.integer "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "food_items", force: :cascade do |t|
    t.string "name"
    t.string "price"
    t.text "desc"
    t.float "rate"
    t.integer "count"
    t.text "structure"
    t.integer "food_kind_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "weight"
  end

  create_table "food_kinds", force: :cascade do |t|
    t.string "name"
    t.integer "food_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "foods", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.string "address"
    t.string "url"
    t.text "description"
    t.string "email"
    t.string "social"
    t.string "work_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "rating"
    t.integer "count"
    t.text "delivery"
    t.string "report_email"
    t.text "sellout"
    t.integer "area_id"
  end

  create_table "forum_group_links", force: :cascade do |t|
    t.integer "forum_group_id"
    t.integer "forum_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "forum_groups", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "forum_messages", force: :cascade do |t|
    t.string "author"
    t.text "body"
    t.integer "user_id"
    t.integer "forum_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "forums", force: :cascade do |t|
    t.string "name"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "author"
    t.integer "user_id"
  end

  create_table "halls", force: :cascade do |t|
    t.string "name"
    t.integer "cinema_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "houses", force: :cascade do |t|
    t.string "title"
    t.integer "street_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lifestyle_group_links", force: :cascade do |t|
    t.integer "lifestyle_id"
    t.integer "lifestyle_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lifestyle_groups", force: :cascade do |t|
    t.string "name"
    t.integer "parent_id", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lifestyles", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.string "address"
    t.text "work_time"
    t.string "description"
    t.string "email"
    t.float "rating"
    t.text "social"
    t.string "report_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "url"
    t.text "sellout"
    t.integer "area_id"
  end

  create_table "media_assets", force: :cascade do |t|
    t.string "url"
    t.integer "mediable_id"
    t.string "mediable_type"
    t.string "kind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "file_name"
    t.integer "file_size"
    t.string "file_status", default: "new"
    t.string "file_location"
    t.string "content_type"
  end

  create_table "movie_sessions", force: :cascade do |t|
    t.datetime "begin_at"
    t.datetime "finish_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "movie_id"
  end

  create_table "movies", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "duration"
    t.string "genres"
    t.string "censorship"
    t.float "rating"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "hall_id"
    t.string "file_name"
    t.integer "file_size"
    t.string "file_status", default: "new"
    t.string "file_location"
    t.string "content_type"
    t.integer "cinema_id"
    t.datetime "premiere"
  end

  create_table "news_items", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.string "source_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.integer "notificable_id"
    t.string "notificable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "professions", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "purchase_good_links", force: :cascade do |t|
    t.integer "purchase_id"
    t.integer "food_item_id"
    t.integer "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "purchases", force: :cascade do |t|
    t.integer "delivery_id"
    t.integer "food_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
  end

  create_table "rates", force: :cascade do |t|
    t.integer "rate"
    t.string "rateable_type"
    t.integer "rateable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
  end

  create_table "recreation_group_links", force: :cascade do |t|
    t.integer "recreation_id"
    t.integer "recreation_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "recreation_groups", force: :cascade do |t|
    t.string "name"
    t.integer "parent_id", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "recreations", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.string "address"
    t.string "url"
    t.text "description"
    t.string "email"
    t.float "rating"
    t.string "social"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "report_email"
    t.text "work_time"
    t.text "sellout"
    t.integer "area_id"
  end

  create_table "reviews", force: :cascade do |t|
    t.integer "user_id"
    t.text "body"
    t.string "reviewable_type"
    t.integer "reviewable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
  end

  create_table "services", force: :cascade do |t|
    t.string "name"
    t.integer "serviceable_id"
    t.string "serviceable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "staffs", force: :cascade do |t|
    t.string "name"
    t.string "surname"
    t.integer "profession_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "staffable_id"
    t.string "staffable_type"
  end

  create_table "stocks", force: :cascade do |t|
    t.datetime "begin_at"
    t.datetime "finish_at"
    t.string "title"
    t.integer "view_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "body"
    t.integer "stockable_id"
    t.string "stockable_type"
    t.string "preview"
    t.text "sellout"
    t.integer "area_id"
  end

  create_table "streets", force: :cascade do |t|
    t.string "title"
    t.integer "area_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subscribes", force: :cascade do |t|
    t.integer "user_id"
    t.integer "subscribable_id"
    t.string "subscribable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tickets", force: :cascade do |t|
    t.string "name"
    t.string "source_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transports", force: :cascade do |t|
    t.string "route_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "phone_number"
    t.string "api_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "firebase_token"
    t.integer "house_id"
  end

  create_table "values", force: :cascade do |t|
    t.string "kind"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
