class CreateAlbums < ActiveRecord::Migration[5.1]
  def change
    create_table :albums do |t|
      t.string :name
      t.string :phone
      t.string :address
      t.string :email
      t.string :social

      t.timestamps
    end
  end
end
