class RenameColumnInDictionaries < ActiveRecord::Migration[5.1]
  def change
    rename_column :dictionaries , :type , :kind
  end
end
