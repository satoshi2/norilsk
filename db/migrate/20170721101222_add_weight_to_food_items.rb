class AddWeightToFoodItems < ActiveRecord::Migration[5.1]
  def change
    add_column :food_items, :weight, :string
  end
end
