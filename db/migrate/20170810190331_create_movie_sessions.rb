class CreateMovieSessions < ActiveRecord::Migration[5.1]
  def change
    create_table :movie_sessions do |t|
      t.datetime :begin_at
      t.datetime :finish_at

      t.timestamps
    end
  end
end
