class CreateLifestyleGroupLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :lifestyle_group_links do |t|
      t.integer :lifestyle_id
      t.integer :lifestyle_group_id

      t.timestamps
    end
  end
end
