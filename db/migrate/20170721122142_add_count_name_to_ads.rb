class AddCountNameToAds < ActiveRecord::Migration[5.1]
  def change
    add_column :ads, :count, :integer
    add_column :ads, :name, :string
  end
end
