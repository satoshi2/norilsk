class RemoveCompanyIdFromStocks < ActiveRecord::Migration[5.1]
  def change
    remove_column :stocks, :company_id, :integer
  end
end
