class AddCounterToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :counter, :integer
  end
end
