class CreateBaners < ActiveRecord::Migration[5.1]
  def change
    create_table :baners do |t|
      t.string :title
      t.string :body
      t.integer :banerable_id
      t.string :banerable_type

      t.timestamps
    end
  end
end
