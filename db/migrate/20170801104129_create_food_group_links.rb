class CreateFoodGroupLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :food_group_links do |t|
      t.integer :food_id
      t.integer :food_group_id

      t.timestamps
    end
  end
end
