class CreateRates < ActiveRecord::Migration[5.1]
  def change
    create_table :rates do |t|
      t.integer :rate
      t.string :rateable_type
      t.integer :reteable_id

      t.timestamps
    end
  end
end
