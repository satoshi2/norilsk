class AddParentTypeToCompanyGroup < ActiveRecord::Migration[5.1]
  def change
    add_column :company_groups, :parent_type, :string
  end
end
