class CreateSubscribes < ActiveRecord::Migration[5.1]
  def change
    create_table :subscribes do |t|
      t.integer :user_id
      t.integer :subscribable_id
      t.string :subscribable_type

      t.timestamps
    end
  end
end
