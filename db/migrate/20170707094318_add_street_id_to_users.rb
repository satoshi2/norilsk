class AddStreetIdToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :street_id, :integer
  end
end
