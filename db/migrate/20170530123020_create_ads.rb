class CreateAds < ActiveRecord::Migration[5.1]
  def change
    create_table :ads do |t|
      t.string :name
      t.string :condition
      t.text :body
      t.string :price
      t.string :number

      t.timestamps
    end
  end
end
