class CreateForumMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :forum_messages do |t|
      t.string :name
      t.text :body
      t.integer :user_id
      t.integer :forum_id

      t.timestamps
    end
  end
end
