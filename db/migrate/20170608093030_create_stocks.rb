class CreateStocks < ActiveRecord::Migration[5.1]
  def change
    create_table :stocks do |t|
      t.datetime :begin_at
      t.datetime :finish_at
      t.string :title
      t.integer :view_count
      t.integer :company_id

      t.timestamps
    end
  end
end
