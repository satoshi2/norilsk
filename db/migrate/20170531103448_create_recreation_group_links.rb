class CreateRecreationGroupLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :recreation_group_links do |t|
      t.integer :recreation_id
      t.integer :recreation_group_id

      t.timestamps
    end
  end
end
