class DropTableAdGroupLink < ActiveRecord::Migration[5.1]
  def change
    drop_table :ad_group_links
  end
end
