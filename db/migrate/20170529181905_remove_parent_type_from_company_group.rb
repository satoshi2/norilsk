class RemoveParentTypeFromCompanyGroup < ActiveRecord::Migration[5.1]
  def change
    remove_column :company_groups, :parent_type, :string
  end
end
