class AddMovieIdToMovieSessions < ActiveRecord::Migration[5.1]
  def change
    add_column :movie_sessions, :movie_id, :integer
  end
end
