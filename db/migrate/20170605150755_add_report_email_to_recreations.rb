class AddReportEmailToRecreations < ActiveRecord::Migration[5.1]
  def change
    add_column :recreations, :report_email, :string
  end
end
