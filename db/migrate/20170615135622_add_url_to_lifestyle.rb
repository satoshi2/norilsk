class AddUrlToLifestyle < ActiveRecord::Migration[5.1]
  def change
    add_column :lifestyles, :url, :string
  end
end
