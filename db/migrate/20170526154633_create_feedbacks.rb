class CreateFeedbacks < ActiveRecord::Migration[5.1]
  def change
    create_table :feedbacks do |t|
      t.integer :user_id
      t.text :description
      t.integer :feedbackable_id
      t.string :feedbackable_type

      t.timestamps
    end
  end
end
