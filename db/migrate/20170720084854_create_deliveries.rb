class CreateDeliveries < ActiveRecord::Migration[5.1]
  def change
    create_table :deliveries do |t|
      t.integer :user_id
      t.integer :house_id
      t.integer :porch
      t.string :code
      t.integer :floor
      t.integer :apartment
      t.string :name

      t.timestamps
    end
  end
end
