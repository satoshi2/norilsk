class CreateResources < ActiveRecord::Migration[5.1]
  def change
    create_table :extra_resources do |t|
      t.string :kind
      t.string :source_url

      t.timestamps
    end
  end
end
