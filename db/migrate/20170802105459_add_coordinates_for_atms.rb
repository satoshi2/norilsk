class AddCoordinatesForAtms < ActiveRecord::Migration[5.1]
  def change
    add_column :atms, :latitude, :float
    add_column :atms, :longtitude, :float
  end
end
