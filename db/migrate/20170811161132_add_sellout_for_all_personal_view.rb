class AddSelloutForAllPersonalView < ActiveRecord::Migration[5.1]
  def change
    add_column :ads , :sellout, :text
    add_column :events , :sellout, :text
    add_column :albums , :sellout, :text
    add_column :cinemas , :sellout, :text
    add_column :companies , :sellout, :text 
    add_column :lifestyles , :sellout, :text
    add_column :recreations , :sellout, :text
    add_column :foods , :sellout, :text
    add_column :stocks , :sellout, :text
  end
end
