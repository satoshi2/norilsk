class AddWorkTimeToCompanies < ActiveRecord::Migration[5.1]
  def change
    add_column :companies, :work_time, :text
  end
end
