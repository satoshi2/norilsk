class AddPublishedUntilToAd < ActiveRecord::Migration[5.1]
  def change
    add_column :ads, :published_until, :datetime
  end
end
