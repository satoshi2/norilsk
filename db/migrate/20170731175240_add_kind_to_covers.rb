class AddKindToCovers < ActiveRecord::Migration[5.1]
  def change
    add_column :covers, :kind, :string
  end
end
