class CreateLifestyles < ActiveRecord::Migration[5.1]
  def change
    create_table :lifestyles do |t|
      t.string :name
      t.string :phone
      t.string :address
      t.text :work_time
      t.string :description
      t.string :email
      t.float :rating
      t.text :social
      t.string :report_email

      t.timestamps
    end
  end
end
