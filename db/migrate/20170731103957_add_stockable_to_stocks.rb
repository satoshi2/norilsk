class AddStockableToStocks < ActiveRecord::Migration[5.1]
  def change
    add_column :stocks, :stockable_id, :integer
    add_column :stocks, :stockable_type, :string
  end
end
