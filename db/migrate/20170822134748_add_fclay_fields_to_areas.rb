class AddFclayFieldsToAreas < ActiveRecord::Migration[5.1]
  def change

    add_column :areas, :file_name, :string
    add_column :areas, :file_size, :integer
    add_column :areas, :file_status, :string, :default => "new"
    add_column :areas, :file_location, :string
    add_column :areas, :content_type, :string

  end


end
