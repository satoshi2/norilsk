class CreateReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :reviews do |t|
      t.integer :user_id
      t.text :body
      t.string :reviewable_type
      t.integer :reviewable_id

      t.timestamps
    end
  end
end
