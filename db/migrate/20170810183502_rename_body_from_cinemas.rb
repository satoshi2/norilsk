class RenameBodyFromCinemas < ActiveRecord::Migration[5.1]
  def change
    rename_column :cinemas, :body, :description
    rename_column :movies, :body, :description
  end
end
