class CreateForumGroupLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :forum_group_links do |t|
      t.integer :forum_group_id
      t.integer :forum_id

      t.timestamps
    end
  end
end
