class CreateFoodKinds < ActiveRecord::Migration[5.1]
  def change
    create_table :food_kinds do |t|
      t.string :name
      t.integer :food_id

      t.timestamps
    end
  end
end
