class AddsDefaultsForParentId < ActiveRecord::Migration[5.1]
  def change
    change_column :ad_groups , :parent_id , :integer , :default => 0
    change_column :atm_groups , :parent_id , :integer , :default => 0
    change_column :company_groups , :parent_id , :integer , :default => 0
    change_column :event_groups , :parent_id , :integer , :default => 0
    change_column :lifestyle_groups , :parent_id , :integer , :default => 0
    change_column :recreation_groups , :parent_id , :integer , :default => 0
  end
end
