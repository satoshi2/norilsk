class AddReportEmailToCompanies < ActiveRecord::Migration[5.1]
  def change
    add_column :companies, :report_email, :string
  end
end
