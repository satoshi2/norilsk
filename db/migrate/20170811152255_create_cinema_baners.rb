class CreateCinemaBaners < ActiveRecord::Migration[5.1]
  def change
    create_table :cinema_baners do |t|
      t.string :name
      t.integer :movie_id

      t.timestamps
    end
  end
end
