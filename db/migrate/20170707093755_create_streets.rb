class CreateStreets < ActiveRecord::Migration[5.1]
  def change
    create_table :streets do |t|
      t.string :title
      t.integer :area_id

      t.timestamps
    end
  end
end
