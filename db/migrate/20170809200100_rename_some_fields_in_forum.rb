class RenameSomeFieldsInForum < ActiveRecord::Migration[5.1]
  def change
    rename_column :forum_messages, :name ,:author
    add_column :forums , :author ,:string
  end
end
