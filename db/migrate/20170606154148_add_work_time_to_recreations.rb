class AddWorkTimeToRecreations < ActiveRecord::Migration[5.1]
  def change
    add_column :recreations, :work_time, :text
  end
end
