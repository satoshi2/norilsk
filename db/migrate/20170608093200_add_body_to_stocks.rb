class AddBodyToStocks < ActiveRecord::Migration[5.1]
  def change
    add_column :stocks, :body, :text
  end
end
