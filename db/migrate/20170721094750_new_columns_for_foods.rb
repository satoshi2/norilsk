class NewColumnsForFoods < ActiveRecord::Migration[5.1]
  def change
    add_column :foods , :count , :integer
    add_column :foods , :deliery , :text
    add_column :foods , :report_email , :string
  end
end
