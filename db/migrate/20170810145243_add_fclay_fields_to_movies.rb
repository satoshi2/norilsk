class AddFclayFieldsToMovies < ActiveRecord::Migration[5.1]
  def change

    add_column :movies, :file_name, :string
    add_column :movies, :file_size, :integer
    add_column :movies, :file_status, :string, :default => "new"
    add_column :movies, :file_location, :string
    add_column :movies, :content_type, :string

  end


end
