class CreateCinemas < ActiveRecord::Migration[5.1]
  def change
    create_table :cinemas do |t|
      t.string :name
      t.string :phone
      t.string :address
      t.string :body
      t.string :email
      t.string :report_email
      t.string :url

      t.timestamps
    end
  end
end
