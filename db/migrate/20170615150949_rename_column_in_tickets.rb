class RenameColumnInTickets < ActiveRecord::Migration[5.1]
  def change
    rename_column :tickets , :route_url , :source_url
  end
end
