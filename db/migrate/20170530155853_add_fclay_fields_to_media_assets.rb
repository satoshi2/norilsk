class AddFclayFieldsToMediaAssets < ActiveRecord::Migration[4.2]
  def change

    add_column :media_assets, :file_name, :string
    add_column :media_assets, :file_size, :integer
    add_column :media_assets, :file_status, :string, :default => "new"
    add_column :media_assets, :file_location, :string
    add_column :media_assets, :content_type, :string

  end


end
