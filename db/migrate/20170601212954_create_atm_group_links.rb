class CreateAtmGroupLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :atm_group_links do |t|
      t.integer :atm_id
      t.integer :atm_group_id

      t.timestamps
    end
  end
end
