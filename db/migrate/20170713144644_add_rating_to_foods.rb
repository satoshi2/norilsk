class AddRatingToFoods < ActiveRecord::Migration[5.1]
  def change
    add_column :foods, :rating, :float
  end
end
