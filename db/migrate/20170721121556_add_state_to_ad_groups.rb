class AddStateToAdGroups < ActiveRecord::Migration[5.1]
  def change
    add_column :ad_groups, :state, :integer, :default => 0
  end
end
