class AddGroupIdToAds < ActiveRecord::Migration[5.1]
  def change
    add_column :ads, :group_id, :integer
  end
end
