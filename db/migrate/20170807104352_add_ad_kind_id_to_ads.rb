class AddAdKindIdToAds < ActiveRecord::Migration[5.1]
  def change
    add_column :ads, :ad_kind_id, :integer
  end
end
