class RenameFieldInAds < ActiveRecord::Migration[5.1]
  def change
    rename_column :ads , :name , :title
  end
end
