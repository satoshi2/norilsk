class CreateCompanyGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :company_groups do |t|
      t.string :name
      t.integer :parent_id

      t.timestamps
    end
  end
end
