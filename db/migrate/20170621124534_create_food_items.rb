class CreateFoodItems < ActiveRecord::Migration[5.1]
  def change
    create_table :food_items do |t|
      t.string :name
      t.string :price
      t.text :desc
      t.float :rate
      t.integer :count
      t.text :structure
      t.integer :food_kind_id

      t.timestamps
    end
  end
end
