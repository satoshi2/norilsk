class CreateAtmKinds < ActiveRecord::Migration[5.1]
  def change
    create_table :atm_kinds do |t|
      t.string :name

      t.timestamps
    end
  end
end
