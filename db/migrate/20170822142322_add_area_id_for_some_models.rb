class AddAreaIdForSomeModels < ActiveRecord::Migration[5.1]
  def change
    add_column :foods, :area_id, :integer
    add_column :companies, :area_id, :integer
    add_column :lifestyles, :area_id, :integer
    add_column :recreations, :area_id, :integer
    add_column :events, :area_id, :integer
    add_column :stocks, :area_id, :integer
    add_column :atms, :area_id, :integer
  end
end
