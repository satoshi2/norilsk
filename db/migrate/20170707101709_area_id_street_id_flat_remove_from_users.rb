class AreaIdStreetIdFlatRemoveFromUsers < ActiveRecord::Migration[5.1]
  def change
    remove_column :users, :street_id, :integer
    remove_column :users, :area_id, :integer
    remove_column :users, :flat, :integer
  end
end
