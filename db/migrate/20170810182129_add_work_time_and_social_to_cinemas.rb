class AddWorkTimeAndSocialToCinemas < ActiveRecord::Migration[5.1]
  def change
    add_column :cinemas, :work_time , :text
    add_column :cinemas, :social , :string
  end
end
