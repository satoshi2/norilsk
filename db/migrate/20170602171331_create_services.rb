class CreateServices < ActiveRecord::Migration[5.1]
  def change
    create_table :services do |t|
      t.string :name
      t.integer :serviceable_id
      t.string :serviceable_type

      t.timestamps
    end
  end
end
