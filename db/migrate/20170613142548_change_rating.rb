class ChangeRating < ActiveRecord::Migration[5.1]
  def change
    change_column :companies , :rating , :float
    change_column :recreations , :rating , :float
  end
end
