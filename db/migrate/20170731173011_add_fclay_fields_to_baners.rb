class AddFclayFieldsToBaners < ActiveRecord::Migration[5.1]
  def change

    add_column :baners, :file_name, :string
    add_column :baners, :file_size, :integer
    add_column :baners, :file_status, :string, :default => "new"
    add_column :baners, :file_location, :string
    add_column :baners, :content_type, :string

  end


end
