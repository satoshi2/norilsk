class AddStaffableToStaffs < ActiveRecord::Migration[5.1]
  def change
    add_column :staffs, :staffable_id, :integer
    add_column :staffs, :staffable_type, :string
  end
end
