class CreateMovies < ActiveRecord::Migration[5.1]
  def change
    create_table :movies do |t|
      t.string :name
      t.text :body
      t.string :duration
      t.string :genres
      t.string :censorship
      t.float :rating

      t.timestamps
    end
  end
end
