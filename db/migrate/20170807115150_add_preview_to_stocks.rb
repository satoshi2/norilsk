class AddPreviewToStocks < ActiveRecord::Migration[5.1]
  def change
    add_column :stocks, :preview, :string
  end
end
