class AddPremiereToMovies < ActiveRecord::Migration[5.1]
  def change
    add_column :movies, :premiere, :datetime
  end
end
