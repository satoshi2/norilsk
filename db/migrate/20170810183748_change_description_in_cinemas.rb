class ChangeDescriptionInCinemas < ActiveRecord::Migration[5.1]
  def change
    change_column :cinemas, :description ,:text
  end
end
