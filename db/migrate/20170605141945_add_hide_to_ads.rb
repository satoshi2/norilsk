class AddHideToAds < ActiveRecord::Migration[5.1]
  def change
    add_column :ads, :hide, :integer
  end
end
