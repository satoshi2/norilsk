class CreateEventGroupLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :event_group_links do |t|
      t.integer :event_id
      t.integer :event_group_id

      t.timestamps
    end
  end
end
