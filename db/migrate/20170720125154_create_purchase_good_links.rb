class CreatePurchaseGoodLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :purchase_good_links do |t|
      t.integer :purchase_id
      t.integer :food_item_id
      t.integer :amount

      t.timestamps
    end
  end
end
