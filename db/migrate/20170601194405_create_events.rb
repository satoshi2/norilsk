class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :name
      t.datetime :begin_at
      t.datetime :finish_at
      t.text :description
      t.string :phone
      t.string :address

      t.timestamps
    end
  end
end
