class AddRatingToCinemas < ActiveRecord::Migration[5.1]
  def change
    add_column :cinemas, :rating, :float
  end
end
