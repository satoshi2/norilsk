class AddPaidToAdGroups < ActiveRecord::Migration[5.1]
  def change
    add_column :ad_groups, :paid, :integer, :default => 0
  end
end
