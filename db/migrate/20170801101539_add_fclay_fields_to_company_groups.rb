class AddFclayFieldsToCompanyGroups < ActiveRecord::Migration[5.1]
  def change

    add_column :company_groups, :file_name, :string
    add_column :company_groups, :file_size, :integer
    add_column :company_groups, :file_status, :string, :default => "new"
    add_column :company_groups, :file_location, :string
    add_column :company_groups, :content_type, :string

  end


end
