class CreateAdLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :ad_group_links do |t|
      t.integer :ad_id
      t.integer :ad_group_id


      t.timestamps
    end
  end
end
