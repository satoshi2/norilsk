class ChangedColumnGroupId < ActiveRecord::Migration[5.1]
  def change
    rename_column :ads ,:group_id , :ad_group_id
  end
end
