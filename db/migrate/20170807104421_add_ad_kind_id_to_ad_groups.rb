class AddAdKindIdToAdGroups < ActiveRecord::Migration[5.1]
  def change
    add_column :ad_groups, :ad_kind_id, :integer
  end
end
