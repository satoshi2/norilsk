class AddFclayFieldsToCovers < ActiveRecord::Migration[5.1]
  def change

    add_column :covers, :file_name, :string
    add_column :covers, :file_size, :integer
    add_column :covers, :file_status, :string, :default => "new"
    add_column :covers, :file_location, :string
    add_column :covers, :content_type, :string

  end


end
