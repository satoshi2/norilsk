class RemoveRatingFromFoods < ActiveRecord::Migration[5.1]
  def change
    remove_column :foods, :rating, :float
  end
end
