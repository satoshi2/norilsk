class CreateDictionaries < ActiveRecord::Migration[5.1]
  def change
    create_table :dictionaries do |t|
      t.string :type
      t.text :body

      t.timestamps
    end
  end
end
