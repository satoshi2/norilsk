class AddHallIdToMovies < ActiveRecord::Migration[5.1]
  def change
    add_column :movies, :hall_id, :integer
  end
end
