class CreateCompanyGroupLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :company_group_links do |t|
      t.integer :company_group_id
      t.integer :company_id

      t.timestamps
    end
  end
end
