class ChangedFieldinModelAd < ActiveRecord::Migration[5.1]
  def change
    rename_column :ads, :number, :phone
  end
end
