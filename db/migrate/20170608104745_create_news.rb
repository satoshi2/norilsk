class CreateNews < ActiveRecord::Migration[5.1]
  def change
    create_table :news_items do |t|
      t.string :title
      t.text :body
      t.string :source_url

      t.timestamps
    end
  end
end
