class CreateFoods < ActiveRecord::Migration[5.1]
  def change
    create_table :foods do |t|
      t.string :name
      t.string :phone
      t.string :address
      t.string :url
      t.text :description
      t.string :email
      t.string :rating
      t.string :social
      t.string :work_time

      t.timestamps
    end
  end
end
