class AddStateToAds < ActiveRecord::Migration[5.1]
  def change
    add_column :ads, :state, :integer
  end
end
