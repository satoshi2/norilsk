class CreatePurchases < ActiveRecord::Migration[5.1]
  def change
    create_table :purchases do |t|
      t.integer :delivery_id
      t.integer :food_id

      t.timestamps
    end
  end
end
