class CreateAtmGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :atm_groups do |t|
      t.string :name
      t.integer :parent_id

      t.timestamps
    end
  end
end
