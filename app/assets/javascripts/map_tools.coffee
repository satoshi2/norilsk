
window.plantain or= {}


class plantain.MapTools

  map: null
  drawingManager: null
  geocoder: null
  marker: null
  center:null
  marker_dragged_callback: null
  geocoderMarker:false
  area: null
  area_changed_callback:null
  success_geocode_callback:null
  failed_geocode_callback:null

  constructor : (@container) ->
    @geocoder = new (google.maps.Geocoder)

  create_map: ->
    mapOptions =
      zoom: 15
      disableDefaultUI: true
    @map = new (google.maps.Map)($("##{@container}")[0], mapOptions)

  create_marker: (lat,lon, marker_dragged_callback = null) ->
    if @marker?
      @marker.setMap(null)
    @marker_dragged_callback = marker_dragged_callback
    @marker = new (google.maps.Marker)(
      position: new (google.maps.LatLng)(lat,lon)
      draggable:@marker_dragged_callback?
      map: @map)
    @marker.addListener('dragend', @marker_dragged)
    if @marker_dragged_callback?
      @marker_dragged_callback(lat,lon)

  marker_dragged: (e) =>
    if @marker_dragged_callback?
      lat = e.latLng.lat()
      lng = e.latLng.lng()
      @marker_dragged_callback(lat,lng)

  geoCode: (address, marker=false, marker_dragged_callback = null, success_geocode_callback = null, failed_geocode_callback = null) ->
    @geocoderMarker = marker
    @success_geocode_callback = success_geocode_callback
    @failed_geocode_callback = failed_geocode_callback
    @marker_dragged_callback = marker_dragged_callback
    @geocoder.geocode { 'address': address }, @codeAdressResult

  createArea: (data,area_changed_callback=null) ->
    @area_changed_callback = area_changed_callback
    regexp = /(\d+\.\d+) (\d+\.\d+)/g
    matches = data.match(regexp)
    return if matches == null
    areaCoords = new Array
    i = 0
    while i < matches.length
      a = matches[i].split(' ')
      areaCoords.push new (google.maps.LatLng)(a[0], a[1])
      i++
    @area = new (google.maps.Polygon)(
      paths: areaCoords
      strokeColor: '#FF0000'
      strokeOpacity: 0.8
      strokeWeight: 2
      fillColor: '#FF0000'
      fillOpacity: 0.35
      geodesic: true
      editable: @area_changed_callback?)
    @area.setMap @map
    @map.fitBounds @area.getBounds()
    if @area_changed_callback?
      google.maps.event.addListener @area.getPath(), 'set_at', @polygonChanged
      google.maps.event.addListener @area.getPath(), 'insert_at', @polygonChanged


  createDrawingManager: (area_changed_callback=null) ->
    @area_changed_callback = area_changed_callback
    @drawingManager = new (google.maps.drawing.DrawingManager)(
      drawingMode: google.maps.drawing.OverlayType.POLYGON
      drawingControl: false
      polygonOptions:
        editable: true
        strokeColor: 'red'
        fillOpacity: 0.2
        strokeWeight: 2
        fillColor: 'red'
      drawingControlOptions:
        position: google.maps.ControlPosition.TOP_CENTER
        drawingModes: [ google.maps.drawing.OverlayType.POLYGON ])
    @drawingManager.setMap @map
    google.maps.event.addListener @drawingManager, 'polygoncomplete', @polygoncomplete


  polygoncomplete: (polygon) =>
    google.maps.event.clearListeners @drawingManager, 'polygoncomplete'
    @drawingManager.setDrawingMode google.maps.drawing.OverlayType.FALSE
    @area = polygon
    if @area_changed_callback?
      @area_changed_callback @area.getPath().getArray()
    google.maps.event.addListener @area.getPath(), 'set_at', @polygonChanged
    google.maps.event.addListener @area.getPath(), 'insert_at', @polygonChanged


  polygonChanged: =>
    if @area_changed_callback?
      @area_changed_callback @area.getPath().getArray()

  set_center: (lat,lng) ->
    @map.setCenter {lat:parseFloat(lat), lng:parseFloat(lng)}

  codeAdressResult: (results, status) =>
    if status == google.maps.GeocoderStatus.OK
      element = results[0].geometry.location
      @center = [element.lat(),element.lng()]
      @map.setCenter element
      if @geocoderMarker == true
        @create_marker(@center[0],@center[1],@marker_dragged_callback)
      if @success_geocode_callback?
        @success_geocode_callback(@center[0],@center[1])

    else
      if @failed_geocode_callback?
        @failed_geocode_callback(status)
      new jBox('Notice', {content: "Адрес не найден", color: 'red'});

  destroyArea: ->
    @area.setMap null if @area?
