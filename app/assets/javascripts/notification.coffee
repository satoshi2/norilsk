processFields = (e) ->
  if $("#notification_kind").val() == 'personal'
    $("#notification_notificable_id_input").show()
    $("#notification_notificable_type_input").show()
  else
    $("#notification_notificable_id_input").hide()
    $("#notification_notificable_type_input").hide()

$ ->
  processFields()
  $("#notification_kind").change processFields
