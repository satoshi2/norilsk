root = exports ? this


$ ->


  $("#atm_address_input").append("<div style='float:right;cursor:pointer;'><a id='geocoder'>Найти</a></div>")

  $("#geocoder").click geocode


geocode = () ->
  address = "Норильск" + " " + $("#atm_address").val()
  new jBox('Notice', {content: "Ищем: #{address}"});
  root.map_tools.geoCode(address,true,marker_dragged)

polygon_changed = (data) ->
  return unless data?
  result = 'POLYGON (('
  cycled_coords = data
  cycled_coords.push data[0]
  cycled_coords.forEach (element, index, array) ->
    result += element.lat().toFixed(6) + ' ' + element.lng().toFixed(6) + ', '
  result = result.substring(0, result.length - 2)
  result += '))'
  $('#city_area_text').val result


marker_dragged = (lat,lng) ->
  $("#resource-center-text").val("#{lat},#{lng}")


root.initialize_view_form = ->
  root.map_tools = new plantain.MapTools("map-canvas")
  root.map_tools.create_map()

  if $(".row-center_text td").length > 0 && $(".row-center_text td").html().length > 0
    center = $(".row-center_text td").html().split(",")
    root.map_tools.set_center(center[0], center[1])
    root.map_tools.create_marker(center[0], center[1])



root.initialize_edit_form = ->
  root.map_tools = new plantain.MapTools("map-canvas")
  root.map_tools.create_map()

  if $("#resource-center-text").length > 0 && $("#resource-center-text").val().length > 0
    center = $("#resource-center-text").val().split(",")
    root.map_tools.set_center(center[0], center[1])
    root.map_tools.create_marker(center[0], center[1],marker_dragged)
  else
    root.map_tools.set_center(69.3498828,88.1971476,17)
