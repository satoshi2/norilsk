node(:error_code) {0}
child @resource => :data do
  node(:area_name){|a| a.house.street.area.title}
  node(:street_name){|a| a.house.street.title}
  node(:house_name){|a| a.house.title}
  node(:area_id){|a| a.house.street.area.id}
  node(:street_id){|a| a.house.street.id}
  node(:house_id){|a| a.house.id}
end
