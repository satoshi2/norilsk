node(:error_code) {0}
child @atms => :data do
    attribute :id, :address
    node(:area_id){|a| a.try(:area_id)}
    node(:category){|a| a.atm_groups.first.try(:name)}
    node(:atm_kind){|a| a.atm_kind.try(:name)}
    node(:lat){|a| a.try(:latitude)}
    node(:lon){|a| a.try(:longtitude)}
end
