node(:error_code) {0}
child @lifestyles => :data do
  attribute :id, :name, :phone, :address, :url, :description, :email, :rating ,:report_email
  node(:area_id){|a| a.try(:area_id)}
  node(:social){|a| a.social_for_api}
  node(:work_time){|a| a.work_time_for_api}
  node(:media){|a| a.media_assets.to_a.map{|f|  f.file_url}}
  node(:services){|a| a.services.map{|f| f.name}}
  child :staffs do
    attribute :name , :surname
    node(:avatar){|a| a.media_assets.first.file_url}
    node(:job){|a| a.profession.name}
  end
end
