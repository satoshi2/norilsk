node(:error_code) {0}
child @recreation => :data do
  attribute :id, :name, :phone, :address, :url, :description, :email, :rating ,:report_email, :sellout
  node(:logo){|a| a.media_assets.where(:kind => "logo").last.try(:file_url)}
  node(:social){|a| a.social_for_api}
  node(:work_time){|a| a.work_time_for_api}
  node(:media){|a| a.media_assets.to_a.map{|f|  f.file_url}}
  node(:services){|a| a.services.map{|f| f.name}}
  child :staffs do
    attribute :name , :surname
    node(:avatar){|a| a.media_assets.first.file_url}
    node(:job){|a| a.profession.name}
  end
  child :stocks do
    attribute :id, :title, :begin_at, :finish_at,:view_count, :body
    node(:company_id){|a| a.stockable_id}
    node(:company_type){|a| a.stockable_type}
    node(:company_name){|a| a.stockable.name}
    node(:image_urls){|a| a.media_assets.to_a.map{|f| ( f.file_url)}}
  end
end
