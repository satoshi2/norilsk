node(:error_code) {0}
child @cinema => :data do
    attribute :id, :name, :phone, :address, :url, :description, :email, :report_email,:rating, :sellout
    node(:logo){|a| a.media_assets.where(:kind => "logo").last.try(:file_url)}
    child :halls do
      attribute :id, :name
    end
    node(:social){|a| a.social_for_api}
    node(:work_time){|a| a.work_time_for_api}
    node(:media){|a| a.media_assets.to_a.map{|f| f.file_url}}
    node(:services){|a| a.services.map{|f| f.name}}
    child :stocks do
      attribute :id, :title, :begin_at, :finish_at,:view_count, :body
      node(:company_id){|a| a.stockable_id}
      node(:company_type){|a| a.stockable_type}
      node(:company_name){|a| a.stockable.name}
      node(:image_urls){|a| a.media_assets.to_a.map{|f| ( f.file_url)}}
    end
end
