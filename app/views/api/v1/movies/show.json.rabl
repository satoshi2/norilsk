node(:error_code) {0}
child @movie => :data do
    attribute :id, :name, :duration, :genres, :censorship, :description, :rating
    node(:premiere){|a| a.premiere.to_i}
    node(:logo){|a| a.file_url}
    node(:cinema_count){@cinema_count}
    node(:review_count){|a| a.reviews.size}
end
