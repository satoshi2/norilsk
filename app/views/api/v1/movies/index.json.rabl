node(:error_code) {0}
child @movies => :data do |f|
    attribute :id, :name, :duration, :genres, :rating, :cinema_id, :hall_id
    node(:premiere){|a| a.premiere.to_i}
    node(:sessions_today){|a| a.sessions_today_shedule_str}
    node(:logo){|a| a.file_url}
    node(:review_count){|a| a.reviews.size}
end
