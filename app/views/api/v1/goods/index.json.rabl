node(:error_code) {0}
child @goods => :data do
  attribute :id, :name, :price, :weight, :desc, :rate, :count, :structure
  node(:urls){|a| a.media_assets.to_a.map{|f|  f.file_url}}
end
