node(:error_code) {0}
child @stock => :data do
    attribute :id, :title, :begin_at, :finish_at,:view_count,:body, :sellout
    node(:logo){|a| a.media_assets.where(:kind => "logo").last.try(:file_url)}
    node(:company_id){|a| a.stockable_id}
    node(:company_type){|a| a.stockable_type}
    node(:company_name){|a| a.stockable.try(:name)}
    node(:image_urls){|a| a.media_assets.to_a.map{|f| ( f.file_url)}}

end
