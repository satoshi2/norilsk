node(:error_code) {0}
child @stocks => :data do
    attribute :id, :title, :preview, :begin_at, :finish_at,:view_count, :body
    node(:area_id){|a| a.try(:area_id)}
    node(:company_id){|a| a.stockable_id}
    node(:company_type){|a| a.stockable_type}
    node(:company_name){|a| a.stockable.try(:name)}
    node(:image_urls){|a| a.media_assets.to_a.map{|f| ( f.file_url)}}
end
