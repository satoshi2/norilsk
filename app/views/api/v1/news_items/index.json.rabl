node(:error_code) {0}
child @news => :data do
  attribute :id, :title,:source_url
  node(:created_at) {|a| a.created_at.to_i}
  node(:image_url){|a| a.media_assets.to_a.map{|f| ( f.file_url)}}
end
