node(:error_code) {0}
child @transports => :data do
  attribute :id, :route_name
  node(:route_url){|a| a.media_assets.to_a.map{|f| ( f.file_url)}}
end
