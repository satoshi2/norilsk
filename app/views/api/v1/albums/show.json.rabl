node(:error_code) {0}
child @album => :data do
  attribute :id, :name , :phone, :address, :email, :count
  node(:review_count){|a| a.reviews.size}
  node(:social){|a| a.social_for_api}
  node(:media){|a| a.albums_media_for_api.to_a }
end
