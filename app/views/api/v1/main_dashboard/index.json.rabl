node(:error_code) {0}
child  :data do
  child :weather do
    node(:sunrise){@sunrise}
    node(:sunset){@sunset}
    node(:temperature){@temperature}
    node(:wind){@wind}
  end
  child @news => :news do
    attribute :title
    node(:subtitle){|a| a.body}
    node(:image){|a| a.file_url}
  end
  child @cinema => :cinema do
    attribute :title
    node(:subtitle){|a| a.body}
    node(:image){|a| a.file_url}
  end
  child @sales => :sales do
    attribute :title
    node(:subtitle){|a| a.body}
    node(:image){|a| a.file_url}
  end
  child @events => :events do
    attribute :title
    node(:subtitle){|a| a.body}
    node(:image){|a| a.file_url}
  end
  child @albums => :albums do
    attribute :title
    node(:subtitle){|a| a.body}
    node(:image){|a| a.file_url}
  end
  child :currencies do
    node(:usd){@usd}
    node(:eur){@eur}
  end
end
