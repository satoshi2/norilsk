node(:error_code) {0}
child @ad => :data do
    attribute :id, :title, :body, :price, :name, :phone, :hide, :state, :status, :count,:sellout
    node(:logo){|a| a.media_assets.where(:kind => "logo").last.try(:file_url)}
    node(:ad_kind){|a| a.ad_kind.try(:name)}
    node(:category){|a| a.ad_group.name}
    node(:published_until) {|a| a.published_until.to_i}
    node(:urls){|a| a.media_assets.to_a.map{|f| f.file_url}}
end
