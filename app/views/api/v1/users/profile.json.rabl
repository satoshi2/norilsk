node(:error_code) {0}
object @user
child @user => :data do
  attributes :phone_number, :firebase_token, :house_id
  node(:api_token){@api_token_to_render} if @api_token_to_render
end
