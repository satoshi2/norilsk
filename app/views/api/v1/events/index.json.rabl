node(:error_code) {0}
child @events => :data do
    attribute :id, :name, :description, :phone, :address ,:counter
    node(:area_id){|a| a.try(:area_id)}
    node(:begin_at) {|a| a.begin_at.to_i}
    node(:finish_at) {|a| a.finish_at.to_i}
end
