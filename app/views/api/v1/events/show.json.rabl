node(:error_code) {0}
child @event => :data do
    attribute :id, :name, :description, :phone, :address , :counter, :sellout
    node(:logo){|a| a.media_assets.where(:kind => "logo").last.try(:file_url)}
    node(:begin_at) {|a| a.begin_at.to_i}
    node(:finish_at) {|a| a.finish_at.to_i}
    child :stocks do
      attribute :id, :title, :begin_at, :finish_at,:view_count, :body
      node(:company_id){|a| a.stockable_id}
      node(:company_type){|a| a.stockable_type}
      node(:company_name){|a| a.stockable.name}
      node(:image_urls){|a| a.media_assets.to_a.map{|f| ( f.file_url)}}
    end
end
