
node(:error_code) {0}
child @forums => :data do
    attribute :id, :name ,:body, :author
    node(:messages_count){|a| a.forum_messages.size}
    node(:created){|a| a.created_at.to_i}
end
