
node(:error_code) {0}
child @forum => :data do
    attribute :id, :name ,:body
    node(:messages_count){|a| a.forum_messages.size}
    node(:created){|a| a.created_at.to_i}
    child :forum_messages do
      attribute :id, :name ,:body, :author
      node(:created){|a| a.created_at.to_i}
    end
end
