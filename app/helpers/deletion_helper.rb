module DeletionHelper
  
  def self.included(k)
    unless k.class == Module
      k.scope(:active,-> {where(:deleted => false)}) 
      k.scope(:deleted,-> {where(:deleted => true)})
    end
  end
  
  def destroy
    update_attribute(:deleted,true)
  end
    
end