module GeoHelper

  def center_parts
    parts = /(-?\d+\.\d+) (-?\d+\.\d+)/.match(self.center.to_s).try(:to_a)
    parts.try(:shift)
    parts
  end

  def lat
    self.latitude
  end

  def lng
    self.longtitude
  end

  def set_area
    self.area = @area_text
  end

  def area_text
    self.area.to_s
  end

  def center_text= str
    parts = str.split(",")
    self.latitude = parts[0]
    self.longtitude = parts[1]
  end

  def center_text
    return nil if !self.latitude || !self.longtitude
    "#{self.latitude},#{self.longtitude}"
  end

  def fetch_center

    return unless self.area.present?
    a = Parking.select("
      ST_Centroid(
        ST_GeomFromText('#{self.area}')
      )
    	As center").limit(1)

    return unless a.first
    self.center = a.first.center.to_s

  end


end
