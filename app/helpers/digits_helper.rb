module DigitsHelper
  def self.fetch_phone_number provider, credentials
     begin

        uri = URI(provider)

        req = Net::HTTP::Get.new(uri)
        req['Authorization'] = credentials
        res = Net::HTTP.start(uri.hostname, uri.port,
      :use_ssl => uri.scheme == 'https') {|http|
          http.request(req)
        }
        data = JSON.parse res.body
        data["phone_number"]

    rescue Exception => e
      return nil
    end

  end
end
