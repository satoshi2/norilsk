module Api
    class ApiController < ApplicationController

      include ApplicationHelper
      include ApiHelper

      # skip_before_action :verify_authenticity_token

      def home
        render :text => "See API specification"
      end

      def digits_credentials_present?
        params["digits"].present? && params["digits"]["credentials"].present? && params["digits"]["provider"].present?
      end
      
      def account_kit_access_token_present?
        params["account_kit"].present? && params["account_kit"]["access_token"].present?
      end

      def try_require_user
        @api_token = request.headers["X-Api-Token"]
        if @api_token
          @current_user = User.where(:api_token => md5(@api_token)).first
        end
      end

      def require_user
        require_api_token
        return unless @api_token
        @current_user = User.where(:api_token => md5(@api_token)).first
        if @current_user
          # @current_user.update_attribute(:last_seen,Time.now)
        else
          restrict_access 1,"Api token is invalid"
        end
      end

      def require_api_token
        @api_token = request.headers["X-Api-Token"]
        restrict_access 5, "Missed api token" unless @api_token
      end

      def restrict_access error_code=999, message="Unknown error", http_code=400
        return if @restricted
        @restricted = true
        warden.try(:custom_failure!)
        render_error_code error_code, message, http_code
      end

      def render_error_code error_code=999, message="Unknown error", http_code=400
        render :json => {:error_code => error_code, :error_message => message}, :status => http_code
      end

    end

end
