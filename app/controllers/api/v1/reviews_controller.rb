module Api
  module V1
    class ReviewsController < Api::ApiController
      before_action :require_user, :except => [:index]


      def index
        if params[:reviewable_id].present? & params[:reviewable_type].present?
          @reviews = Review.where(:reviewable_id => params[:reviewable_id] ).where(:reviewable_type => params[:reviewable_type])
        else
          return restrict_access 3,"Cant find reviews"
        end
      end

      def create
        @review = Review.create(ad_params)
        @review.user_id = @current_user.id
        if @review.save
          render :json => {:error_code => 0}
        else
          render :json => {:error_code => 3, :error_desc => @review.errors.full_messages}
        end
      end

      def destroy
        @review = Review.find(params[:id])
        if @review.destroy
          render :json => {:error_code => 0}
        else
          render :json =>{:error_code => 3 , :error_desc => lp.errors.full_messages}
        end
      end
      private

      def ad_params
        params.require(:review).permit(:title, :body, :reviewable_type, :reviewable_id)
      end
    end
  end
end
