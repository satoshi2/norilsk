module Api
  module V1
    class EventsController < Api::ApiController
        def index
          if params[:group_id].present?
            @events_group = EventGroup.where(:id => params[:group_id]).first
            return restrict_access 3,"Cant find events_group" unless @events_group
            @events = @events_group.events
          elsif params[:filter_id].present?
            @events = Event.where(:area_id => params[:filter_id])
          else
            @events = Event.all
          end
        end
        def show
          @event = Event.find(params[:id])
          @event.increment(:counter)
          @event.save
        end



    end
  end
end
