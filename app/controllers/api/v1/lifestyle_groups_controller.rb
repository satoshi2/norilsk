module Api
  module V1
    class LifestyleGroupsController < Api::ApiController
        def index
          if params[:parent_id].present?
            @lifestyle_groups = LifestyleGroup.where(:parent_id => params[:parent_id])
            return restrict_access 3,"Cant find ad_group" unless @lifestyle_groups
          else
            @lifestyle_groups = LifestyleGroup.where(:parent_id => nil)
          end
        end
    end
  end
end
