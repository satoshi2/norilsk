module Api
  module V1
    class PurchaseController < Api::ApiController

      before_action :require_user

      def create
        return restrict_access 3,"Wrong params" if !params[:delivery_id].present? || !params[:restaurant_id].present? || !params[:purchase][:objects].present?
        purchase = Purchase.create(:user_id => @current_user.id ,:delivery_id => params[:delivery_id], :food_id => params[:restaurant_id], :objects =>  params[:purchase][:objects])
        if purchase.persisted?
          render :json => {:error_code => 0}
        else
          render :json => {error_code: 2, desc: purchase.errors.full_messages.to_s}
        end
      end


    end
  end
end
