module Api
  module V1
    class MoviesController < Api::ApiController
        def index
          @movies = Movie.includes(:movie_sessions).joins(:movie_sessions).where("movie_sessions.begin_at > ?", DateTime.now).where("movie_sessions.finish_at < ?", DateTime.now.end_of_day)
        end
        def show
          @movie = Movie.find(params[:id])
          @cinema_count = Cinema.joins(:movies).where(:movies => {:name => @movie.name}).size
        end
    end
  end
end
