module Api
  module V1
    class SearchController < Api::ApiController
        def index
          if params[:pattern].present?
            restaurant = Food.where("name LIKE ?", "%#{params[:pattern]}%")
            restaurant_menu = Food.joins(:food_kinds).where("food_kinds.name LIKE ?", "%#{params[:pattern]}%")
            ads = Ad.where("title LIKE ?", "%#{params[:pattern]}%")
            ads_kind = Ad.joins(:ad_kind).where("ad_kinds.name LIKE ?", "%#{params[:pattern]}%")
            company = Company.where("name LIKE ?", "%#{params[:pattern]}%")
            company_service = Company.joins(:services).where("services.name LIKE ?","%#{params[:pattern]}%")
            lifestyle = Lifestyle.where("name LIKE ?", "%#{params[:pattern]}%")
            lifestyle_service = Lifestyle.joins(:services).where("services.name LIKE ?","%#{params[:pattern]}%")
            recreation = Recreation.where("name LIKE ?", "%#{params[:pattern]}%")
            recreation_service = Recreation.joins(:services).where("services.name LIKE ?","%#{params[:pattern]}%")
            cinema = Cinema.where("name LIKE ?", "%#{params[:pattern]}%")
            cinema_movies = Cinema.joins(:movies).where("movies.name LIKE ?","%#{params[:pattern]}%")
            events = Event.where("name LIKE ?", "%#{params[:pattern]}%")
            atm = Atm.joins(:atm_kind).where("atm_kinds.name LIKE ?", "%#{params[:pattern]}%")
            atm_group = Atm.joins(:atm_groups).where("atm_groups.name LIKE ?", "%#{params[:pattern]}%")
            forum = Forum.where("name LIKE ?", "%#{params[:pattern]}%")
            albums = Album.joins(:services).where("services.name LIKE ?", "%#{params[:pattern]}%")
            stocks = Stock.where("title LIKE ?", "%#{params[:pattern]}%")

            @pattern = restaurant+restaurant_menu+ads+ads_kind+company+company_service+lifestyle+lifestyle_service+recreation+recreation_service+cinema+albums+stocks+cinema_movies+forum+events+atm+atm_group
          end
        end

    end
  end
end
