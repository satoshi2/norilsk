module Api
  module V1
    class AddressToDeliveryController < Api::ApiController

      before_action :require_user

      def index
        @deliveries = Delivery.where(:user_id=> @current_user.id)
      end

      def create
        @resource  = Delivery.new
        @resource.user = @current_user
        @resource.house_id = params[:user][:house_id]
        @resource.porch = params[:user][:delivery_porch]
        @resource.code = params[:user][:delivery_code]
        @resource.floor = params[:user][:delivery_floor]
        @resource.apartment = params[:user][:delivery_apartment]
        @resource.name = params[:user][:name]
        if @resource.save
          render :create
        else
          render :json => {:error_code => 2, :error_desc => resource.errors.full_messages}
        end
      end

      def update
        @delivery = Delivery.find(params[:id])
        begin
          @delivery.update_attributes(user_params) if params[:user].present?
        rescue Exception => e
          Rails.logger.error "Error updating:" + e.message
        end
        render :update
      end

      def destroy
        @delivery = Delivery.find(params[:id])
        if @delivery.destroy
          render :json => {:error_code => 0}
        else
          render :json =>{:error_code => 3 , :error_desc => @delivery.errors.full_messages}
        end
      end

      private

      def user_params
        params.require(:user).permit( :house_id, :delivery_porch, :delivery_code, :delivery_floor, :delivery_apartment , :name )
      end

    end
  end
end
