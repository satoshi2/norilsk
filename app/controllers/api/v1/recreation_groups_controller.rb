module Api
  module V1
    class RecreationGroupsController < Api::ApiController
        def index
          if params[:parent_id].present?
            @recreation_groups = RecreationGroup.where(:parent_id => params[:parent_id])
            return restrict_access 3,"Cant find ad_group" unless @recreation_groups
          else
            @recreation_groups = RecreationGroup.where(:parent_id => nil)
          end
        end
    end
  end
end
