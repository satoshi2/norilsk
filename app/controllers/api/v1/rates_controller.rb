module Api
  module V1
    class RatesController < Api::ApiController
      before_action :require_user


      def index
        return restrict_access 3,"Need params" unless params[:rate].present?
        if Rate.where(:user_id => @current_user.id).where(:rateable_type =>params[:rate][:rateable_type]).where(:rateable_id =>params[:rate][:rateable_id].to_i).first
          render :json => {:error_code => 0, :status => 1}
        else
          render :json => {:error_code => 0, :status => 0}
        end
      end

      def create
        @rate = Rate.create(rate_params)
        @rate.user_id = @current_user.id
        if @rate.save
          render :json => {:error_code => 0}
        else
          render :json => {:error_code => 3, :error_desc => @rate.errors.full_messages}
        end
      end

      private

      def rate_params
        params.require(:rate).permit(:rate, :rateable_type, :rateable_id)
      end
    end
  end
end
