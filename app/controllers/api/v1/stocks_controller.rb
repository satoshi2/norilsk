module Api
  module V1
    class StocksController < Api::ApiController

      def index
        if params[:filter_id].present?
          @stocks = Stock.where(:area_id => params[:filter_id])
        else
          @stocks = Stock.all
        end
      end

      def show
        @stock= Stock.find(params[:id])
        @stock.increment(:view_count)
        @stock.save
        if @stock.present?
          @stock
        else
          return restrict_access 3,"Cant find stock" unless @stocks
        end
      end

    end
  end
end
