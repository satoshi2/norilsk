module Api
  module V1
    class NewsItemsController < Api::ApiController

      def index
        @news = NewsItem.all
        if @news.present?
          @news
        else
          return restrict_access 3,"Cant find event_groups" unless @news
        end
      end

    end
  end
end
