module Api
  module V1
    class MainDashboardController < Api::ApiController
        def index
          # weather
          @sunrise = Value.where(:kind => "sunrise").last.try(:value)
          @sunset = Value.where(:kind => "sunset").last.try(:value)
          @temperature = Value.where(:kind => "temperature").last.try(:value)
          @wind = Value.where(:kind => "wind").last.try(:value)

          #NewsItem
          @news = Cover.where(:kind => "NewsItem")

          # Cinema
          @cinema = Cover.where(:kind => "Cinema")

          # Stock
          @sales = Cover.where(:kind => "Stock")

          # Event

          @events = Cover.where(:kind => "Event")

          # Album

          @albums = Cover.where(:kind => "Event")

          # Values

          @usd = Value.where(:kind => "usd").last.try(:value)
          @eur = Value.where(:kind => "eur").last.try(:value)
        end

    end
  end
end
