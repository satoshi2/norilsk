module Api
  module V1
    class ForumMessageController < Api::ApiController
      before_action :require_user

      def create
        @forum_message = ForumMessage.create(message_params)
        @forum_message.user_id = @current_user.id
        if @forum_message.save
          render :json => {:error_code => 0}
        else
          render :json => {:error_code => 3, :error_desc => @forum_message.errors.full_messages}
        end
      end

      def destroy
        @forum_message = ForumMessage.find(params[:id])
        if @forum_message.destroy
          render :json => {:error_code => 0}
        else
          render :json =>{:error_code => 3 , :error_desc => @forum_message.errors.full_messages}
        end
      end
      private

      def message_params
        params.require(:forum_message).permit(:author, :body, :forum_id)
      end
    end
  end
end
