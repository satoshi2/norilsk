module Api
  module V1
    class UsersController < Api::ApiController

      before_action :require_user, :except => [:login]

      def login
        return restrict_access 1,"Missed Account Kit access_token and Digits credentials" if !account_kit_access_token_present? && !digits_credentials_present?

        if params[:account_kit].present?
          phone_number = AccountKitHelper.fetch_phone_number params[:account_kit][:access_token]
        elsif params[:digits].present?
          phone_number = DigitsHelper.fetch_phone_number params[:digits][:provider], params[:digits][:credentials]
        end

        return restrict_access 1,"Error while determining phone number" unless phone_number

        @user = User.where(:phone_number => phone_number).first_or_create
        token = generate_token
        @user.update_attribute(:api_token,token[:md5])
        @api_token_to_render = token[:raw]

        if @user.house_id.present?
          area = @user.house.street.area.title
          street = @user.house.street.title
          house = @user.house.title
          render :json => { "error_code" => 0, :'api-token' =>  @api_token_to_render ,:'address'=> area+" "+street+" "+house}
        else
          render :json => { "error_code" => 0, :'api-token' =>  @api_token_to_render }
        end
      end

      def address
        if params[:area_id].present?
          @select = Street.where(:area_id => params[:area_id])
        elsif params[:street_id].present?
          @select = House.where(:street_id => params[:street_id])
        else
          @select = Area.all
        end
      end


      def update
        @user = @current_user
        begin
          @user.update_attributes(user_params) if params[:user].present?
        rescue Exception => e
          Rails.logger.error "Error updating:" + e.message
        end
        render :profile
      end

      private

      def user_params
        params.require(:user).permit(:firebase_token , :house_id)
      end


    end
  end
end
