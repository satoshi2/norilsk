module Api
  module V1
    class AtmsController < Api::ApiController

      def index
        if params[:group_id].present?
          @atms_group = AtmGroup.where(:id => params[:group_id]).first
          return restrict_access 3,"Cant find atm_group" unless @atms_group
          @atms = @atms_group.atms
        elsif params[:filter_id].present?
          @atms = Atm.where(:area_id => params[:filter_id])
        else
          @atms = Atm.all
        end
      end


    end
  end
end
