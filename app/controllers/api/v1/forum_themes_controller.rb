module Api
  module V1
    class ForumThemesController < Api::ApiController

      before_action :require_user, :only => [:create]

      def index
        if params[:group_id].present?
          @forum_group = ForumGroup.where(:id => params[:group_id]).first
          return restrict_access 3,"Cant find forum group" unless @forum_group
          @forums = @forum_group.forums
        else
          @forums = Forum.all
        end
      end

      def show
        @forum = Forum.find(params[:id])
      end



      def create
        forum_themes = Forum.new
        forum_themes.user_id = @current_user.id
        forum_themes.name = params[:forum_themes][:name]
        forum_themes.body = params[:forum_themes][:body]
        forum_themes.author = params[:forum_themes][:author]
        if forum_themes.save
          link = ForumGroupLink.new
          link.forum_id = forum_themes.id
          link.forum_group_id = params[:forum_themes][:forum_group_id]
          if link.save
            render :json => {:error_code => 0}
          else
            render :json => {:error_code => 3, :error_desc => link.errors.full_messages}
          end
        else
          render :json => {:error_code => 3, :error_desc => forum_themes.errors.full_messages}
        end
      end


    end
  end
end
