module Api
  module V1
    class RestaurantsController < Api::ApiController



      def index
        if params[:category_id].present?
            @food_group = FoodGroup.where(:id => params[:category_id]).first
            return restrict_access 3,"Cant find restaurant category" unless @food_group
            @foods = @food_group.foods
        elsif params[:filter_id].present?
          @foods = Food.where(:area_id => params[:filter_id])
        else
          @foods = Food.all
        end
      end

      def show
        @food = Food.find(params[:id])
        @food.increment(:count)
        @food.save
        if @food.present?
          @food
        else
          return restrict_access 3,"Cant find restaurant" unless @stock
        end
      end

    end
  end
end
