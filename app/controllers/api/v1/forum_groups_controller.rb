module Api
  module V1
    class ForumGroupsController < Api::ApiController
        def index
          @forum_groups = ForumGroup.all
        end
    end
  end
end
