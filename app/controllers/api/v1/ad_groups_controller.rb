module Api
  module V1
    class AdGroupsController < Api::ApiController
        def index
          if params[:parent_id].present?
            @ad_groups = AdGroup.where(:parent_id => params[:parent_id])
            return restrict_access 3,"Cant find ad_group" unless @ad_groups
          else
            @ad_groups = AdGroup.where(:parent_id => nil)
          end

        end

    end
  end
end
