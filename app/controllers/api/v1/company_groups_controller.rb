module Api
  module V1
    class CompanyGroupsController < Api::ApiController
        def index
          if params[:parent_id].present?
            @company_groups = CompanyGroup.where(:parent_id => params[:parent_id])
            return restrict_access 3,"Cant find ad_group" unless @company_groups
          else
            @company_groups = CompanyGroup.where(:parent_id => nil)
          end
        end
    end
  end
end
