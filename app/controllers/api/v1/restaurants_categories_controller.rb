module Api
  module V1
    class RestaurantsCategoriesController < Api::ApiController


      def index
        if params[:parent_id].present?
          @food_group = FoodGroup.where(:parent_id => params[:parent_id])
          return restrict_access 3,"Cant find ad_group" unless @food_group
        else
          @food_group = FoodGroup.where(:parent_id => nil)
        end
      end

    end
  end
end
