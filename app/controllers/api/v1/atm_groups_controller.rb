module Api
  module V1
    class AtmGroupsController < Api::ApiController
        def index
          if params[:parent_id].present?
            @atm_groups = AtmGroup.where(:parent_id => params[:parent_id])
            return restrict_access 3,"Cant find ad_group" unless @atm_groups
          else
            @atm_groups = AtmGroup.where(:parent_id => nil)
          end
        end
    end
  end
end
