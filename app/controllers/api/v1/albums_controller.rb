module Api
  module V1
    class AlbumsController < Api::ApiController

      def index
        @albums = Album.all
        if @albums.present?
          @albums
        else
          return restrict_access 3,"Cant find albums" unless @albums
        end
      end

      def show
        @album= Album.find(params[:id])
        @album.increment(:count)
        @album.save
        @album_assets = @album.media_assets
      end

    end
  end
end
