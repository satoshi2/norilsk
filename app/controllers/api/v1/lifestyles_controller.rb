module Api
  module V1
    class LifestylesController < Api::ApiController
        def index
          if params[:group_id].present?
              @lifestyle_group = LifestyleGroup.where(:id => params[:group_id]).first
              return restrict_access 3,"Cant find lifestyle_group" unless @lifestyle_group
              @lifestyles = @lifestyle_group.lifestyles
          elsif params[:filter_id].present?
            @lifestyles = Lifestyle.where(:area_id => params[:filter_id])
          else
            @lifestyles = Lifestyle.all
          end
        end

        def show
          @lifestyle = Lifestyle.find(params[:id])
        end

    end
  end
end
