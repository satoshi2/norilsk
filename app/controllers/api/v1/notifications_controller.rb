module Api
  module V1
    class NotificationsController < Api::ApiController

      before_action :require_user

      def index
        total = Notification.where(:notificable_id => nil ).where(:notificable_type => nil)
        user = Notification.where(:notificable_id => @current_user.id).where(:notificable_type => "User")
        @notifications = total+user
      end


    end
  end
end
