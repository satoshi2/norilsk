module Api
  module V1
    class RecreationsController < Api::ApiController
        def index
          if params[:group_id].present?
              @recreation_group = RecreationGroup.where(:id => params[:group_id]).first
              return restrict_access 3,"Cant find recreation_group" unless @recreation_group
              @recreations = @recreation_group.recreations
          elsif params[:filter_id].present?
            @recreations = Recreation.where(:area_id => params[:filter_id])
          else
            @recreations = Recreation.all
          end
        end

        def show
          @recreation = Recreation.find(params[:id])
        end

    end
  end
end
