module Api
  module V1
    class DictionariesController < Api::ApiController

    def index
      @dictionaries = Dictionary.all
    end


      private

      def dictionary_params
        params.require(:dictionary).permit(:kind, :body)
      end
    end
  end
end
