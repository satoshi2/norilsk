module Api
  module V1
    class EmergenciesController < Api::ApiController

      def index
        @emergencies = Emergency.all
        if @emergencies.present?
          @emergencies
        else
          return restrict_access 3,"Cant find emergencies" unless @emergencies
        end
      end

      def show
        @emergency= Emergency.find(params[:id])
        if @emergency.present?
          @emergency
        else
          return restrict_access 3,"Cant find emergency" unless @emergency
        end
      end

    end
  end
end
