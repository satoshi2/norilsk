module Api
  module V1
    class CompaniesController < Api::ApiController
        def index
          if params[:group_id].present?
              @companies_group = CompanyGroup.where(:id => params[:group_id]).first
              return restrict_access 3,"Cant find company_group" unless @companies_group
              @companies = @companies_group.companies
          elsif params[:filter_id].present?
            @companies = Company.where(:area_id => params[:filter_id])
          else
            @companies = Company.all
          end
        end
        def show
          @company = Company.find(params[:id])
        end
    end
  end
end
