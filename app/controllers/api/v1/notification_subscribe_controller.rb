module Api
  module V1
    class NotificationSubscribeController < Api::ApiController

      before_action :require_user

      def create
        return restrict_access 3,"Cant find ad_params" unless params[:id].present? || params[:type].present?
        @subc = Subscribe.new
        @subc.user_id = @current_user.id
        @subc.subscribable_id = params[:id]
        @subc.subscribable_type = params[:type]
        if @subc.save
          render :json => {:error_code => 0}
        else
          render :json => {:error_code => 3, :error_desc => @subc.errors.full_messages}
        end

      end



    end
  end
end
