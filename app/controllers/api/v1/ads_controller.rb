module Api
  module V1
    class AdsController < Api::ApiController

        before_action :require_user, :only => [:create ,:destroy]

        def create
          return restrict_access 3,"Cant find ad_params" unless params[:ad].present?
          @ad = Ad.create(ad_params)
          @ad.user_id = @current_user.id
          if @ad.save
            render :json => {:error_code => 0}
          else
            render :json => {:error_code => 3, :error_desc => @ad.errors.full_messages}
          end
        end

        def index
          if params[:group_id].present?
            @ads = Ad.where(:ad_group_id => params[:group_id]).where(:status => "active")
            return restrict_access 3,"Cant find ad_group" unless @ads
          else
            @ads = Ad.all.where(:status => "active")
          end
        end

        def show
          if Ad.find(params[:id])
            @ad = Ad.find(params[:id])
            @ad.increment(:count)
            @ad.save
          else
            return restrict_access 3,"Cant find ad"
          end
        end

        def destroy
          @ad = Ad.find(params[:id])
          @ad.status = "blocked"
          if @ad.save
            render :json => {:error_code => 0}
          else
            render :json => {:error_code => 3, :error_desc => @ad.errors.full_messages}
          end
        end




        private

        def ad_params
          params.require(:ad).permit(:ad_group_id, :title, :name, :state, :ad_kind_id, :hide, :body, :price, :phone, :publishing_days, :media => [] )
        end
    end
  end
end
