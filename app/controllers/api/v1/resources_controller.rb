module Api
  module V1
    class ResourcesController < Api::ApiController

      def index
        @resources = ExtraResource.all
        if @resources.present?
          @resources
        else
          return restrict_access 3,"Cant find resources" unless @resources
        end
      end


    end
  end
end
