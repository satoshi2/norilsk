module Api
  module V1
    class TransportsController < Api::ApiController

      def index
        @transports = Transport.all
        if @transports.present?
          @transports
        else
          return restrict_access 3,"Cant find transports" unless @transports
        end
      end

      def show
        @transport= Transport.find(params[:id])
        if @transport.present?
          @transport
        else
          return restrict_access 3,"Cant find transport" unless @transport
        end
      end

    end
  end
end
