module Api
  module V1
    class EventGroupsController < Api::ApiController
        def index
          if params[:parent_id].present?
            @event_groups = EventGroup.where(:parent_id => params[:parent_id])
            return restrict_access 3,"Cant find ad_group" unless @event_groups
          else
            @event_groups = EventGroup.where(:parent_id => nil)
          end
        end
    end
  end
end
