module Api
  module V1
    class CinemaController < Api::ApiController
        def index
          if params[:movie].present?
            @cinemas = Cinema.joins(:movies).where(:movies => {:name => params[:movie]})
          else
            @cinemas = Cinema.all
          end
        end
        def show
          @cinema = Cinema.find(params[:id])
        end
    end
  end
end
