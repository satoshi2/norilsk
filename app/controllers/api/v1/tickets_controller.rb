module Api
  module V1
    class TicketsController < Api::ApiController

      def index
        @tickets = Ticket.all
        if @tickets.present?
          @tickets
        else
          return restrict_access 3,"Cant find tickets" unless @tickets
        end
      end


    end
  end
end
