module Api
  module V1
    class ExchangesController < Api::ApiController

      def index
        @exchanges = Exchange.all
        if @exchanges.present?
          @exchanges
        else
          return restrict_access 3,"Cant find exchanges" unless @exchanges
        end
      end


    end
  end
end
