module Api
  module V1
    class GoodsController < Api::ApiController

      def index
        if params[:category_id].present?
          @goods = FoodItem.where(:food_kind_id => params[:category_id])
        end
      end

      def show
        @good = Food.find(params[:id])
        @good.increment(:count)
        @good.save
        if @food.present?
          @food
        else
          return restrict_access 3,"Cant find restaurant" unless @stock
        end
      end
      
    end
  end
end
