class SiteController < ApplicationController
  
  before_action :fetch_text, :only => [:rules, :guide, :about]
  
  def home
  end
  
  def rules 
    render :text_block
  end
  
  def guide
    render :text_block
  end
  
  def about
    render :text_block
  end
  
  private 
  
  def fetch_text
    @text = TextBlock.where(:key => params[:action]).first
  end
  
  
end
