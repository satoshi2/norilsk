ActiveAdmin.register LifestyleGroup do
  menu :parent => 'Справочник Образ жизни', :label => "Группы образов жизни"

  show do
    columns do
      column do
        attributes_table do
          row :name
          row :parent_id
        end
      end
      if !resource.parent_id
        column do
          panel "Media" do
            render :partial => "admin/shared/media_assets", locals: {:resource => resource}
          end
        end
      end
    end
  end
end
