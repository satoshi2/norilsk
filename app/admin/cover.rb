ActiveAdmin.register Cover do
  menu :parent => 'Реклама главная страница', :label => "Покрытие"
  form do |f|
     f.inputs "" do
       f.input :kind
       f.input :title
       f.input :body
       f.input :file, as: :file
     end
     f.actions
   end
end
