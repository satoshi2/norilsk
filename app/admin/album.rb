ActiveAdmin.register Album do

  menu :label => "Фотоальбомы"

  form do |f|
       f.inputs  do
         input :name
         input :phone
         input :sellout
         input :address
         input :email
         f.input :image, as: :file
       end
       f.inputs do
         input :vk ,:label =>"Вконтакте"
         input :instagram ,:label =>"Инстаграм"
         input :facebook ,:label =>"Facebook"
         input :ok ,:label =>"Одноклассники"
       end
       f.actions
     end
  show do
    columns do
      column do
        attributes_table do
          row :name
          row :phone
          row :sellout
          row :address
          row :email
          row :social
        end
        panel "Albums " do
          render :partial => "admin/shared/albums", locals: {:resource => resource}
        end
      end
      column do
        panel "Albums style" do
          render :partial => "admin/shared/service", locals: {:resource => resource}
        end
      end
    end
  end
end
