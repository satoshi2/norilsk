ActiveAdmin.register RecreationGroupLink do

  menu :parent => 'Справочник Развлечения и Отдых', :label => "Связи развлечений и отдыха"

  controller do

    def create
      super do |success,failure|
        success.html { redirect_to return_path(resource) }
        failure.html { redirect_to return_path(resource), :notice => resource.errors.full_messages }
      end
    end

    def destroy
      super do |success,failure|
        success.html {  redirect_to return_path(resource) }
      end
    end

    def return_path resource
      public_send("admin_recreation_path", resource.recreation)
    end

  end

end
