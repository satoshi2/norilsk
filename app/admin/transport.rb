ActiveAdmin.register Transport do
  menu :label => "Справочник Транспорты"

  show do
    columns do
      column do
        attributes_table do
          row :id
          row :route_name
        end

      end
      column do
        panel "Картинки" do
          render :partial => "admin/shared/media_assets", locals: {:resource => resource}
        end
      end
    end
  end
end
