ActiveAdmin.register Purchase do
  menu :parent => 'Справочник Заказы', :label => "Заказы"

  show do
    columns do
      column do
        attributes_table do
          row :delivery_id
          row :food_id
          row :user_id
          row :created_at
          row :updated_at
        end
      end
      column do
        panel "Объекты заказа" do
          render :partial => "admin/shared/purchase_good_link", locals: {:resource => resource}
        end
      end
    end
  end

end
