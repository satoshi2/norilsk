ActiveAdmin.register AtmGroupLink do
  menu :parent => 'Справочник Банкоматы', :label => "Связь Банкоматов и Групп Банкоматов"
  controller do

    def create
      super do |success,failure|
        success.html { redirect_to return_path(resource) }
        failure.html { redirect_to return_path(resource), :notice => resource.errors.full_messages }
      end
    end

    def destroy
      super do |success,failure|
        success.html {  redirect_to return_path(resource) }
      end
    end

    def return_path resource
      public_send("admin_atm_path", resource.atm)
    end

  end

end
