ActiveAdmin.register MovieSession do
  menu :parent => 'Справочник Кинотеатры', :label => "Сеансы фильмов"
  controller do
      def new
        build_resource
        resource.movie_id = params[:movie_id]
      end
      def create
        super do |success,failure|
          success.html { redirect_to return_path(resource) }
          failure.html { redirect_to return_path(resource), :notice => resource.errors.full_messages }
        end
      end

      def destroy
        super do |success,failure|
          success.html {  redirect_to return_path(resource) }
        end
      end

      def return_path resource
        public_send("admin_movie_path", resource.movie.id)
      end
  end

end
