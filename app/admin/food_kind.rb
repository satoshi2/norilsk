ActiveAdmin.register FoodKind do
  menu :parent => 'Справочник Рестораны', :label => "Типы еды"

  controller do

      def create
        super do |success,failure|
          success.html { redirect_to return_path(resource) }
          failure.html { redirect_to return_path(resource), :notice => resource.errors.full_messages }
        end
      end

      def destroy
        super do |success,failure|
          success.html {  redirect_to return_path(resource) }
        end
      end

      def return_path resource
        public_send("admin_food_path", resource.food)
      end

    end

    show do
      columns do
        column do
          attributes_table do
            row :name
          end
        end
        column do
          panel "Food Items" do
            render :partial => "admin/shared/food_item", locals: {:resource => resource}
          end
        end
      end
    end

end
