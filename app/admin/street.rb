ActiveAdmin.register Street do


  menu :label => "Улицы"

  form do |f|
     f.inputs "" do
       f.input :area_id, as: :select,  :collection => Area.all.map{|a| [a.title,a.id]}
       f.input :title
     end
     f.actions
   end

end
