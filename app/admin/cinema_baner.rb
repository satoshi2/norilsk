ActiveAdmin.register CinemaBaner do
  menu :parent => 'Справочник Кинотеатры', :label => "Рекламные банеры"
  form do |f|
     f.inputs "" do
       f.input :name
       f.input :movie
     end
     f.actions
   end

end
