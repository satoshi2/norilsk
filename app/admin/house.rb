ActiveAdmin.register House do
  menu :label => "Дома"
  form do |f|
     f.inputs "" do
       f.input :street_id, as: :select,  :collection => Street.all.map{|a| [a.title,a.id]}
       f.input :title
     end
     f.actions
   end
end
