ActiveAdmin.register Ad do

  menu :parent => 'Справочник Объявления', :label => "Объявления"

  scope :active ,default: true
  scope :blocked

  member_action :activate, method: :put do
   resource.activate
   redirect_to admin_ads_path, notice: "Объявление разблокировано"
 end

  index do
    column :id
    column :title
    column :body
    column :ad_group
    column :name
    column :phone
    column :price
    column :published_until
    column "Статус" do |f|
      case f.status
        when "active"
          status_tag "Активен", :ok
        when "blocked"
          status_tag "Заблокирован"
      end
    end
    column "Зарегистрирован" do |f|
      f.created_at
    end
    column :actions do |object|
      if object.status == 'active'
          raw( %(
            #{link_to "View", [:admin, object]}
            #{(link_to"Edit", [:edit, :admin, object])}&nbsp;&nbsp;
            #{link_to "Заблокировать", [:admin, object], method: :delete}) )
      else
        raw( %(
          #{link_to "Разблокировать", activate_admin_ad_path(object), method: :put}) )
      end
    end
  end

  form do |f|
    f.inputs "Ad" do
      f.input :ad_kind
      f.input :ad_group
      f.input :title
      f.input :body
      f.input :sellout
      f.input :user
      f.input :name
      f.input :phone
      f.input :price
      f.input :publishing_days
      f.input :status
      f.input :hide
      f.input :state
      f.input :media ,as: :file
    end
    f.actions
  end

  show do
    columns do
      column do
        attributes_table do
          row :title
          row :ad_kind
          row :ad_group
          row :body
          row :sellout
          row :price
          row :name
          row :phone
          row :published_until
          row :status
          row :hide
          row :state
          row :count
        end
      end
      column do
        panel "Media Assets" do
          render :partial => "admin/shared/media_assets", locals: {:resource => resource}
        end
      end

    end
  end


end
