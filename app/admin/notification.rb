ActiveAdmin.register Notification do
  menu :label => "Уведомления"

    controller do
      before_action { @page_title = "Уведомления" }
    end

    index do
      selectable_column
      column :id
      column "Название" do |f|
        f.title
      end
      column "Текст" do |f|
        f.body
      end
      column "Дата создания" do |f|
        f.created_at
      end
      actions
    end

    form do |f|
      f.inputs "Details", :multipart => true do
        f.input :kind, as: :select, :collection => %w(total personal).map{|a| [I18n.t("notification.kind.#{a}"),a]}, :include_blank => false
        f.input :notificable_id
        f.input :notificable_type
        f.input :title
        f.input :body
      end
      f.actions
    end

end
