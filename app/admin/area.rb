ActiveAdmin.register Area do
  menu :label => "Районы"


  index do
    column :id
    column :title
    column :created_at
    column :file_url
    actions
  end

  form do |f|
    f.inputs "Area" do
      f.input :title
      f.input :file, as: :file
    end
    f.actions
  end


  show do
    columns do
      column do
        attributes_table do
          row :id
          row :title
          row :file do |f|
            image_tag(f.file_url, :width => 250)
          end
        end
      end
    end
  end

end
