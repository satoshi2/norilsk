ActiveAdmin.register Forum do
  menu :parent => 'Форум', :label => "Темы форума"

  show do
    columns do
      column do
        attributes_table do
          row :name
          row :body
        end
      end
      column do
        panel "Forum groups" do
          render :partial => "admin/shared/forum", locals: {:resource => resource}
        end
      end
    end
  end
end
