ActiveAdmin.register Ticket do
  menu :label => "Справочник Авиа ЖД Билеты"

  show do
    columns do
      column do
        attributes_table do
          row :id
          row :name
          row :source_url
        end

      end
      column do
        panel "Картинки" do
          render :partial => "admin/shared/media_assets", locals: {:resource => resource}
        end
      end
    end
  end
end
