ActiveAdmin.register Event do

  menu :parent => 'Справочник События', :label => "События"
  show do
    columns do
      column do
        attributes_table do
          row :area
          row :name
          row :phone
          row :address
          row :sellout
          row :url
          row :description
          row :email
          row :rating
          row :social
        end
      end
      column do
        panel "Media Assets" do
          render :partial => "admin/shared/media_assets", locals: {:resource => resource}
        end
      end
      column do
        panel "EventGroup" do
          render :partial => "admin/shared/event", locals: {:resource => resource}
        end
      end
    end
  end

end
