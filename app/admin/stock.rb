ActiveAdmin.register Stock do

  menu :label => "Акции"

  form do |f|
     f.inputs "" do
       f.input :area
       f.input :title
       f.input :preview
       f.input :sellout
       f.input :body
       f.input :begin_at
       f.input :finish_at
       f.input :stockable_id
       f.input :stockable_type
     end
     f.actions
    end

  show do
    columns do
      column do
        attributes_table do
          row :area
          row :id
          row :title
          row :preview
          row :sellout
          row :begin_at
          row :finish_at
          row :view_count
          row :stockable
          row :body
        end

      end
      column do
        panel "Картинки" do
          render :partial => "admin/shared/media_assets", locals: {:resource => resource}
        end
      end
    end
  end
end
