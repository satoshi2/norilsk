ActiveAdmin.register FoodGroupLink do

  menu :parent => 'Справочник Рестораны', :label => "Связи ресторанов и групп ресторанов"

  controller do

    def create
      super do |success,failure|
        success.html { redirect_to return_path(resource) }
        failure.html { redirect_to return_path(resource), :notice => resource.errors.full_messages }
      end
    end

    def destroy
      super do |success,failure|
        success.html {  redirect_to return_path(resource) }
      end
    end

    def return_path resource
      public_send("admin_food_path", resource.food)
    end

  end

end
