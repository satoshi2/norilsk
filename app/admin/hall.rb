ActiveAdmin.register Hall do

  menu :parent => 'Справочник Кинотеатры', :label => "Залы кинотеатров"
  controller do

    def create
      super do |success,failure|
        success.html { redirect_to return_path(resource) }
        failure.html { redirect_to return_path(resource), :notice => resource.errors.full_messages }
      end
    end

    def destroy
      super do |success,failure|
        success.html {  redirect_to return_path(resource) }
      end
    end

    def return_path resource
      public_send("admin_cinema_path", resource.cinema)
    end

  end

  show do
    columns do
      column do
        attributes_table do
          row :name
          row :cinema
        end
      end
      column do
        panel "Movies" do
          render :partial => "admin/halls/movies", locals: {:resource => resource}
        end
      end
    end
  end
end
