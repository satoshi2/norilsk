ActiveAdmin.register Food do

  menu :parent => 'Справочник Рестораны', :label => "Рестораны"

  index do
    column :id
    column :name
    actions
  end

  form do |f|
       f.inputs  do
         input :area
         input :name
         input :phone
         input :address
         input :url
         input :description
         input :sellout
         input :email
         input :report_email
         input :rating
         input :delivery
         f.input :image, as: :file
       end
       f.inputs do
         input :vk ,:label =>"Вконтакте"
         input :instagram ,:label =>"Инстаграм"
         input :facebook ,:label =>"Facebook"
         input :ok ,:label =>"Одноклассники"
       end
       columns do
         column do
           panel "Рабочее время" do
             f.inputs do
               input :mon_wt ,:label =>"Понедельник"
             end
             f.inputs do
               input :tue_wt ,:label =>"Вторник"
             end
             f.inputs do
               input :wend_wt ,:label =>"Среда"
             end
             f.inputs do
               input :thur_wt ,:label =>"Четверг"
             end
             f.inputs do
               input :fri_wt ,:label =>"Пятница"
             end
             f.inputs do
               input :sat_wt ,:label =>"Суббота"
             end
             f.inputs do
               input :sun_wt ,:label =>"Воскресенье"
             end
           end
         end
         column do
           panel "Обеденное время" do
             f.inputs do
               input :mon_dt ,:label =>"Понедельник"
             end
             f.inputs do
               input :tue_dt ,:label =>"Вторник"
             end
             f.inputs do
               input :wend_dt ,:label =>"Среда"
             end
             f.inputs do
               input :thur_dt ,:label =>"Четверг"
             end
             f.inputs do
               input :fri_dt ,:label =>"Пятница"
             end
             f.inputs do
               input :sat_dt ,:label =>"Суббота"
             end
             f.inputs do
               input :sun_dt ,:label =>"Воскресенье"
             end
           end
         end
        end
       f.actions
     end

  show do
    columns do
      column do
        attributes_table do
          row :area
          row :name
          row :phone
          row :address
          row :url
          row :description
          row :sellout
          row :email
          row :report_email
          row :delivery
          row :count
          row :rating
          row :social
          row :work_time
        end
        panel "Media " do
          render :partial => "admin/shared/media_assets", locals: {:resource => resource}
        end
      end
      column do
        panel "Staff" do
          render :partial => "admin/shared/staff", locals: {:resource => resource}
        end
        panel "Food Kind" do
          render :partial => "admin/shared/food_kind", locals: {:resource => resource}
        end
      end
      column do
        panel "Company groups" do
          render :partial => "admin/shared/food", locals: {:resource => resource}
        end
        panel "Services" do
          render :partial => "admin/shared/service", locals: {:resource => resource}
        end
      end
    end
  end

end
