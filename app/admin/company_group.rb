ActiveAdmin.register CompanyGroup do
  menu :parent => 'Справочник Компании', :label => "Группы Компании"
  form do |f|
     f.inputs "" do
       f.input :name
       f.input :parent_id
       f.input :file, as: :file
     end
     f.actions
   end

end
