ActiveAdmin.register Staff do
  menu :parent => 'Сотрудники', :label => "Сотрудники"

  filter :profession_id, as: :select
  controller do

      def new
        build_resource
        resource.staffable_type = params[:staffable_type]
        resource.staffable_id = params[:staffable_id]
      end
      def create
        super do |success,failure|
          success.html { redirect_to return_path(resource) }
          failure.html { redirect_to return_path(resource), :notice => resource.errors.full_messages }
        end
      end

      def destroy
        super do |success,failure|
          success.html {  redirect_to return_path(resource) }
        end
      end

      def return_path resource
        public_send("admin_#{resource.staffable_type.underscore}_path", resource.staffable)
      end
    end

  form do |f|
     f.inputs "" do
       f.input :name
       f.input :surname
       f.input :profession_id, as: :select,  :collection => Profession.all.map{|a| [a.name,a.id]}
       f.hidden_field :staffable_id
       f.hidden_field :staffable_type
       f.input :avatar, as: :file
     end
     f.actions
    end

end
