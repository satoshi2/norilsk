ActiveAdmin.register FoodItem do
  menu :parent => 'Справочник Рестораны', :label => "Еда ресторанов"

  controller do
      def new
        build_resource
        resource.food_kind_id = params[:food_kind_id]
      end
      def create
        super do |success,failure|
          success.html { redirect_to return_path(resource) }
          failure.html { redirect_to return_path(resource), :notice => resource.errors.full_messages }
        end
      end

      def destroy
        super do |success,failure|
          success.html {  redirect_to return_path(resource) }
        end
      end

      def return_path resource
        public_send("admin_food_kind_path", resource.food_kind.id)
      end
  end

  form do |f|
   f.inputs  do
     f.hidden_field :food_kind_id
     input :name
     input :price
     input :weight
     input :desc
     input :rate
     input :structure
     input :url, as: :file, input_html: { multiple: true }
    #  f.li "(Выбирайте несколько файлов сразу)"
   end
   f.actions
  end

  show do
    columns do
      column do
        attributes_table do
          row :name
          row :price
          row :weight
          row :desc
          row :rate
          row :count
          row :structure
        end

      end
      column do
        panel "Media " do
          render :partial => "admin/shared/media_assets", locals: {:resource => resource}
        end
      end
    end
  end
end
