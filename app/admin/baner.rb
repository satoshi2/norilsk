ActiveAdmin.register Baner do
  menu :parent => 'Реклама главная страница', :label => "Рекламные банеры"
  form do |f|
     f.inputs "" do
       f.input :title
       f.input :body
       f.input :banerable_id
       f.input :banerable_type
       f.input :file, as: :file
     end
     f.actions
   end

end
