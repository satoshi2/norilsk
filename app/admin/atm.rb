ActiveAdmin.register Atm do
  menu :parent => 'Справочник Банкоматы', :label => "Банкоматов"
  show do
    columns do
      column do
        attributes_table do
          row :address
        end
      end
      column do
        panel "AtmGroup" do
          render :partial => "admin/shared/atm", locals: {:resource => resource}
        end
      end
    end
  end

  form do |f|
    div :style => "float:right;width:49%" do
      render :partial => "map_widget", :locals => {:action => "edit"}
    end
    div :style => "width:49%;" do
         f.inputs "" do
           f.input :area
           f.input :address
           f.input :atm_kind_id, as: :select,  :collection => AtmKind.all.map{|a| [a.name,a.id]}
           f.input :center_text, :input_html => {id: "resource-center-text"}
         end
         f.actions
       end
    end

    show  do |f|
      div :style => "float:right;width:49%" do
        render :partial => "map_widget", :locals => {:action => "view"}
      end

      div :style => "width:49%;" do
        attributes_table do
          row :area
          row :address
          row :atm_kind_id
          row :center_text
        end
      end
    end
end
