ActiveAdmin.register NewsItem do

  menu :label => "Новости"

  show do
    columns do
      column do
        attributes_table do
          row :id
          row :title
          row :body
          row :created_at
          row :source_url
        end

      end
      column do
        panel "Картинки" do
          render :partial => "admin/shared/media_assets", locals: {:resource => resource}
        end
      end
    end
  end
end
