ActiveAdmin.register Service do
  menu :label => "Услуги"

  actions :all, :except => [:new,:edit,:update,:show]
  controller do

      def create
        super do |success,failure|
          success.html { redirect_to return_path(resource) }
          failure.html { redirect_to return_path(resource), :notice => resource.errors.full_messages }
        end
      end

      def destroy
        super do |success,failure|
          success.html {  redirect_to return_path(resource) }
        end
      end

      def return_path resource
        public_send("admin_#{resource.serviceable_type.underscore}_path", resource.serviceable)
      end

    end

end
