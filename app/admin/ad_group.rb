ActiveAdmin.register AdGroup do
  menu :parent => 'Справочник Объявления', :label => "Группы объявлений"

  form do |f|
    f.inputs "Ad" do
      f.input :parent
      f.input :ad_kind
      f.input :name
      f.input :state
      f.input :pay ,as: :check_boxes
    end
    f.actions
  end

end
