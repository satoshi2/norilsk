ActiveAdmin.register CompanyGroupLink do
  menu :parent => 'Справочник Компании', :label => "Связь компаний и групп компаний"
  controller do

    def create
      super do |success,failure|
        success.html { redirect_to return_path(resource) }
        failure.html { redirect_to return_path(resource), :notice => resource.errors.full_messages }
      end
    end

    def destroy
      super do |success,failure|
        success.html {  redirect_to return_path(resource) }
      end
    end

    def return_path resource
      public_send("admin_company_path", resource.company)
    end

  end

end
