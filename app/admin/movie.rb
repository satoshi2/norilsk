ActiveAdmin.register Movie do

  menu :parent => 'Справочник Кинотеатры', :label => "Фильмы"

  index do
    column :id
    column :name
    column :hall
    column :cinema
    actions
  end
  controller do
      def new
        build_resource
        resource.hall_id = params[:hall_id]
        resource.cinema_id = params[:hall_id]
      end
      def create
        super do |success,failure|
          success.html { redirect_to return_path(resource) }
          failure.html { redirect_to return_path(resource), :notice => resource.errors.full_messages }
        end
      end

      def destroy
        super do |success,failure|
          success.html {  redirect_to return_path(resource) }
        end
      end

      def return_path resource
        public_send("admin_hall_path", resource.hall.id)
      end
  end


  form do |f|
   f.inputs  do
     f.hidden_field :hall_id
     f.hidden_field :cinema_id
     input :name
     input :premiere
     input :description
     input :duration
     input :genres
     input :censorship
     input :rating
     input :file, as: :file
   end
   f.actions
  end

  show do
    columns do
      column do
        attributes_table do
          row :file do |f|
            image_tag(f.file_url, :width => 250)
          end
          row :cinema
          row :hall
          row :name
          row :premiere
          row :description
          row :duration
          row :genres
          row :censorship
          row :rating
        end
      end
      column do
        panel "Movie Sessions" do
          render :partial => "admin/movies/movie_sessions", locals: {:resource => resource}
        end
      end
    end
  end
end
