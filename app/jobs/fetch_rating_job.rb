class FetchRatingJob < ApplicationJob
  queue_as :default

  def perform(id)
    a = Rate.where(:id => id).first
    return unless a
    b = a.rateable
    b.fetch_rating
  end
  
end
