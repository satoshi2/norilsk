class Review < ApplicationRecord
  belongs_to :reviewable, polymorphic: true ,optional:true
  belongs_to :user
  validates_presence_of  :reviewable_id, :reviewable_type , :body , :user_id
end
