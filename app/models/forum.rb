class Forum < ApplicationRecord
  has_many :forum_group_links
  has_many :forum_groups, :through => :forum_group_links
  has_many :forum_messages
  belongs_to :user
  validates_presence_of :name, :body ,:author
end
