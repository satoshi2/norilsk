class MovieSession < ApplicationRecord
  belongs_to :movie
  validates_presence_of :begin_at, :finish_at

  def shedule_str
    {begin_at: begin_at.strftime("%H:%M"),finish_at: finish_at.strftime("%H:%M")}
  end
end
