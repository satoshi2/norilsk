class Hall < ApplicationRecord
  belongs_to :cinema
  has_many :movies
  validates_presence_of :name
end
