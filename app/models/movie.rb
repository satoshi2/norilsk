class Movie < ApplicationRecord
  belongs_to :hall
  belongs_to :cinema
  has_fclay_attachment
  has_many :reviews, as: :reviewable
  has_many :movie_sessions
  has_many :cinema_baners
  validates_presence_of :name, :description, :duration, :genres, :censorship,:premiere

  def sessions_today_shedule_str
    movie_sessions.where("begin_at > ?", DateTime.now).where("finish_at < ?", DateTime.now.end_of_day).map{|a| a.shedule_str}
  end
end
