class RecreationGroupLink < ApplicationRecord
  belongs_to :recreation
  belongs_to :recreation_group
  validates_presence_of :recreation_id , :recreation_group_id
end
