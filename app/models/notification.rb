class Notification < ApplicationRecord
  belongs_to :notificable, polymorphic: true, required: false
  validates_presence_of :title, :body
  attr_accessor :kind
  before_save :check_kind



  private

  def check_kind
    self.notificable_id = nil if %w(total).include? self.kind
    self.notificable_type = nil if %w(total).include? self.kind
  end
end
