class EventGroupLink < ApplicationRecord
  belongs_to :event
  belongs_to :event_group
  validates_presence_of :event_id , :event_group_id
end
