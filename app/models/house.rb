class House < ApplicationRecord
  belongs_to :street
  has_many :users
  has_many :deliveries
end
