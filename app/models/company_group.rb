class CompanyGroup < ApplicationRecord

  has_many :company_group_links
  has_many :companies, through: :company_group_links
  has_fclay_attachment
  belongs_to :parent, class_name: "CompanyGroup", optional: true
  validates_presence_of :name




end
