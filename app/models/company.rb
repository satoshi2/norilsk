class Company < ApplicationRecord

  belongs_to :area , optional: true
  has_many :company_group_links
  has_many :company_groups, :through => :company_group_links
  has_many :baners, as: :banerable

  has_many :media_assets, as: :mediable
  has_many :services, as: :serviceable
  has_many :staffs, as: :staffable
  has_many :rates, as: :rateable
  has_many :reviews, as: :reviewable
  has_many :stocks, as: :stockable
  validates_presence_of :name, :phone, :address, :url, :description, :email, :report_email

  include RatingHelper
  def work_time_for_api
    {"monday":{"working_time": mon_wt,"dinner_time": mon_dt},
    "tuesday":{"working_time": tue_wt,"dinner_time": tue_dt},
    "wednesday":{"working_time": wend_wt,"dinner_time": wend_dt},
    "thursday":{"working_time": thur_wt,"dinner_time": thur_wt},
    "friday":{"working_time": fri_wt,"dinner_time": fri_dt},
    "saturday":{"working_time": sat_wt,"dinner_time": sat_wt},
    "sunday":{"working_time": sat_wt,"dinner_time": sat_wt}}
  end
  def social_for_api
    {"vk": vk,"instagram": instagram, "facebook": facebook, "Одноклассники": ok}
  end

  def media_assets_kinds
    %w(logo main menu)
  end

  store :work_time, accessors: [ :mon_wt, :mon_dt,:tue_wt, :tue_dt,:wend_wt, :wend_dt,:thur_wt, :thur_dt,:fri_wt, :fri_dt,:sat_wt, :sat_dt,:sun_wt, :sun_dt ], coder: JSON
  store :social, accessors: [ :vk , :instagram , :facebook, :ok ], coder: JSON
end
