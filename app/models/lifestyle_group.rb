class LifestyleGroup < ApplicationRecord
  has_many :lifestyle_group_links
  has_many :lifestyles, through: :lifestyle_group_links
  has_many :media_assets, as: :mediable
  belongs_to :parent, class_name: "LifestyleGroup", optional: true
  validates_presence_of :name
end
