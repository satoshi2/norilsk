class AdKind < ApplicationRecord
  has_many :ads
  has_many :ad_groups
  validates_presence_of :name
end
