class Staff < ApplicationRecord
  belongs_to :profession
  has_many :media_assets, as: :mediable, dependent: :destroy
  belongs_to :staffable, polymorphic: true
  validates_presence_of :name , :surname , :profession_id ,:staffable
  attr_writer :avatar
  after_create :fetch_avatar
  def fetch_avatar
    return unless @avatar
    a = MediaAsset.new(:file => @avatar  ,:kind => "avatar", :mediable_type =>self.class.to_s ,:mediable_id =>self.id )
    a.save
  end
end
