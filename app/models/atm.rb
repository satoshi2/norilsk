class Atm < ApplicationRecord

  include GeoHelper
  attr_accessor :city
  has_many :atm_group_links
  belongs_to :area , optional:true
  has_many :atm_groups, :through => :atm_group_links
  belongs_to :atm_kind
  validates_presence_of :address
end
