class Transport < ApplicationRecord
  has_many :media_assets, as: :mediable
  validates_presence_of :route_name 
end
