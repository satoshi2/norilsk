class LifestyleGroupLink < ApplicationRecord
  belongs_to :lifestyle
  belongs_to :lifestyle_group
  validates_presence_of :lifestyle_id , :lifestyle_group_id
end
