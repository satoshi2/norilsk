class Cover < ApplicationRecord
  has_fclay_attachment
  validates_presence_of :title, :body, :kind
end
