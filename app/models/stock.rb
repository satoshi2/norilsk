class Stock < ApplicationRecord
  has_many :media_assets, as: :mediable
  belongs_to :stockable, polymorphic: true
  belongs_to :area , optional: true
  validates_presence_of :title ,:begin_at ,:finish_at, :body

  def media_assets_kinds
    %w(logo main menu)
  end

end
