class Rate < ApplicationRecord
  belongs_to :rateable, polymorphic: true
  belongs_to :user
  validates_inclusion_of :rate, :in => 1..5
  validates_presence_of  :rateable_type ,:rateable_id, :rate
  after_create :change_rating

  def change_rating

    FetchRatingJob.perform_later self.id
  end
end
