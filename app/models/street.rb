class Street < ApplicationRecord
  belongs_to :area
  has_many :houses
end
