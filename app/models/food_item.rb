class FoodItem < ApplicationRecord

  has_many :purchase_good_links
  has_many :purchase, through: :purchase_good_links
  has_many :media_assets, as: :mediable
  belongs_to :food_kind
  has_many :rates, as: :rateable
  attr_writer :url
  after_create :fetch_url

  def fetch_url
    return unless @url
    @url.each do |f|
      a = MediaAsset.new(:file => f , :kind =>"food item", :mediable_type =>self.class.to_s ,:mediable_id =>self.id )
      a.save
    end
  end
end
