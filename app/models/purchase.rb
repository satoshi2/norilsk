class Purchase < ApplicationRecord
  belongs_to :delivery
  belongs_to :food

  has_many :purchase_good_links
  has_many :food_items, through: :purchase_good_links

  attr_writer :objects


  after_create :create_objects


  private

  def create_objects
    @objects.each do |f|
      PurchaseGoodLink.create(:purchase_id => self.id, :food_item_id => f["id"], :amount => f["count"])
    end

  end
end
