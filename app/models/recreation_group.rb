class RecreationGroup < ApplicationRecord
  has_many :recreation_group_links
  has_many :recreations, through: :recreation_group_links

  belongs_to :parent, class_name: "RecreationGroup", optional: true
  validates_presence_of :name
end
