class AtmGroup < ApplicationRecord

  has_many :atm_group_links
  has_many :atms, through: :atm_group_links

  belongs_to :parent, class_name: "AtmGroup", optional: true
  validates_presence_of :name
end
