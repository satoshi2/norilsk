class EventGroup < ApplicationRecord
  has_many :event_group_links
  has_many :events, through: :event_group_links

  belongs_to :parent, class_name: "EventGroup", optional: true
  validates_presence_of :name

end
