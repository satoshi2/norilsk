class Subscribe < ApplicationRecord
  validates_presence_of :user_id , :subscribable_id, :subscribable_type
  belongs_to :subscribable, polymorphic: true , optional: true
  belongs_to :user
end
