class Dictionary < ApplicationRecord
  
  validates_presence_of :kind , :body

  def rules
    {"type": kind ,"body": body}
  end
end
