class FoodGroup < ApplicationRecord
  has_many :food_group_links
  has_many :foods, through: :food_group_links
  has_fclay_attachment
  belongs_to :parent, class_name: "FoodGroup", optional: true
  validates_presence_of :name

end
