class Ad < ApplicationRecord
  belongs_to :ad_group
  belongs_to :user
  belongs_to :ad_kind
  has_many :media_assets, as: :mediable
  validates_inclusion_of :state, :in => 0..1
  validates_inclusion_of :hide, :in => 0..1
  validates_presence_of :title, :published_until, :ad_group_id, :state, :price, :body, :phone, :name
  attr_accessor :publishing_days
  attr_writer :media
  after_create :fetch_media
  before_validation :fetch_publishing_until

  def media_assets_kinds
    %w(logo main menu)
  end

  def fetch_publishing_until
    return unless @publishing_days
    self.published_until = Time.now + @publishing_days.to_i.days
  end

  def fetch_media
    return unless @media
    @media.each do |f|
      a = MediaAsset.new(:file => f ,:mediable_type =>"Ad" ,:mediable_id =>self.id )
      a.save
    end
  end

  scope :blocked, -> { where(status:"blocked") }
  scope :active, -> { where(status:"active") }

  def activate
    update_attributes(:status => "active")
  end

  def destroy
    update_attributes(:status => "blocked")
  end


end
