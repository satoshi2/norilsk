class FoodKind < ApplicationRecord
  belongs_to :food
  has_many :food_items
end
