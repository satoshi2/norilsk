class ForumGroupLink < ApplicationRecord
  belongs_to :forum
  belongs_to :forum_group
  validates_presence_of :forum_id , :forum_group_id
end
