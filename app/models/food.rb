class Food < ApplicationRecord

  belongs_to :area , optional: true
  
  has_many :food_group_links
  has_many :food_groups, :through => :food_group_links
  has_many :media_assets, as: :mediable
  has_many :services, as: :serviceable
  has_many :staffs, as: :staffable
  has_many :rates, as: :rateable
  has_many :reviews, as: :reviewable
  has_many :stocks, as: :stockable
  has_many :food_kinds
  has_many :subscribes, as: :subscribable
  has_many :baners, as: :banerable
  has_many :purchases
  has_many :deliveries, through: :purchases

  validates_presence_of :name, :phone, :address, :url, :description, :email
  attr_writer :image
  after_create :fetch_image

  include RatingHelper

  def image
    media_assets.where(:kind => "logo").first.try(:file_url)
  end

  def fetch_image
    return unless @image
    a = MediaAsset.new(:file => @image  ,:kind => "logo", :mediable_type =>self.class.to_s ,:mediable_id =>self.id )
    a.save
  end

  def work_time_for_api
    {"monday":{"working_time": mon_wt,"dinner_time": mon_dt},
    "tuesday":{"working_time": tue_wt,"dinner_time": tue_dt},
    "wednesday":{"working_time": wend_wt,"dinner_time": wend_dt},
    "thursday":{"working_time": thur_wt,"dinner_time": thur_wt},
    "friday":{"working_time": fri_wt,"dinner_time": fri_dt},
    "saturday":{"working_time": sat_wt,"dinner_time": sat_wt},
    "sunday":{"working_time": sat_wt,"dinner_time": sat_wt}}
  end
  def social_for_api
    {"vk": vk,"instagram": instagram, "facebook": facebook, "Одноклассники": ok}
  end

  def media_assets_kinds
    %w(logo main menu)
  end

  store :work_time, accessors: [ :mon_wt, :mon_dt,:tue_wt, :tue_dt,:wend_wt, :wend_dt,:thur_wt, :thur_dt,:fri_wt, :fri_dt,:sat_wt, :sat_dt,:sun_wt, :sun_dt ], coder: JSON
  store :social, accessors: [ :vk , :instagram , :facebook , :ok ], coder: JSON
end
