class NewsItem < ApplicationRecord
  has_many :media_assets, as: :mediable
  validates_presence_of :title, :body, :source_url
end
