class ForumGroup < ApplicationRecord
  has_many :forum_group_links
  has_many :forums, through: :forum_group_links
  validates_presence_of :name
end
