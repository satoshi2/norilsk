class Baner < ApplicationRecord
  has_fclay_attachment
  validates_presence_of :title, :body
  belongs_to :banerable, polymorphic: true

end
