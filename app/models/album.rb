class Album < ApplicationRecord
  has_many :media_assets, as: :mediable
  has_many :services, as: :serviceable
  has_many :reviews, as: :reviewable
  validates_presence_of :name, :phone, :address, :email
  attr_writer :image
  after_create :fetch_image

  def image
    media_assets.where(:kind => "image").first.try(:file_url)
  end

  def fetch_image
    return unless @image
    a = MediaAsset.new(:file => @image  ,:kind => "image", :mediable_type =>self.class.to_s ,:mediable_id =>self.id )
    a.save
  end

  def media_assets_kinds
    %w(wedding weather image)
  end
  def albums_media_for_api
    services.all.map do |f|
      {"name": f.name,"assets": MediaAsset.where(:mediable_id => self.id).where(:mediable_type => "Album").where(:kind => f.name).to_a.map{|a| a.file_url}.to_a}
    end
  end

  def social_for_api
    {"vk": vk,"instagram": instagram, "facebook": facebook, "Одноклассники": ok}
  end

  store :social, accessors: [ :vk , :instagram , :facebook, :ok ], coder: JSON

end
