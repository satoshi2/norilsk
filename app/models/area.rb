class Area < ApplicationRecord
  has_many :streets
  has_many :foods
  has_many :companies
  has_many :lifestyles
  has_many :recreations
  has_many :events
  has_many :stocks
  has_many :atms


  has_fclay_attachment
end
