class AtmKind < ApplicationRecord
  has_many :atms
  validates_presence_of :name
end
