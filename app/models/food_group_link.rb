class FoodGroupLink < ApplicationRecord
  belongs_to :food
  belongs_to :food_group
  validates_presence_of :food_id , :food_group_id
end
