class PurchaseGoodLink < ApplicationRecord
  belongs_to :purchase
  belongs_to :food_item
  validates_presence_of :amount
end
