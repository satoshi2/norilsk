class MediaAsset < ApplicationRecord

  belongs_to :mediable, polymorphic: true
  validates_presence_of  :mediable , :file
  has_fclay_attachment
end
