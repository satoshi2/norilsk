class AdGroup < ApplicationRecord
  has_many :ads
  belongs_to :ad_kind, optional: true
  belongs_to :parent, class_name: "AdGroup", optional: true
  validates_inclusion_of :paid, :in => 0..1
  validates_presence_of :name,:pay
  attr_accessor :pay
  before_save :fetch_paid

  def fetch_paid
    if self.pay.last == "true"
      self.paid = 1
    else
      self.paid = 0
    end
  end

end
