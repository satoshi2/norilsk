class User < ApplicationRecord
  belongs_to :house, required: false
  has_many :deliveries
  has_many :reviews
  has_many :ads
  has_many :forums
  has_many :rates
  validates_presence_of :phone_number
end
