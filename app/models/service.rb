class Service < ApplicationRecord
  belongs_to :serviceable, polymorphic: true
  validates_presence_of  :serviceable , :name
end
