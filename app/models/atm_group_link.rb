class AtmGroupLink < ApplicationRecord
  belongs_to :atm
  belongs_to :atm_group
  validates_presence_of :atm_id , :atm_group_id
end
