class Emergency < ApplicationRecord
  validates_presence_of :title, :phone
end
