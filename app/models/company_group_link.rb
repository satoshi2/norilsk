class CompanyGroupLink < ApplicationRecord
  belongs_to :company
  belongs_to :company_group
  validates_presence_of :company_id , :company_group_id
end
