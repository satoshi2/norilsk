class Event < ApplicationRecord

  belongs_to :area , optional: true
  
  has_many :event_group_links
  has_many :event_groups, :through => :event_group_links
  has_many :baners, as: :banerable

  has_many :media_assets, as: :mediable
  has_many :reviews, as: :reviewable
  has_many :stocks, as: :stockable
  validates_presence_of :name , :begin_at, :finish_at, :description, :phone, :address

  def media_assets_kinds
    %w(logo main menu)
  end
end
