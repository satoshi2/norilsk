class Delivery < ApplicationRecord
  belongs_to :user
  belongs_to :house

  has_many :purchases
  has_many :foods, through: :purchases
end
