class Ticket < ApplicationRecord
  has_many :media_assets, as: :mediable
  validates_presence_of :name , :source_url
  def image_url
    media_assets.first.try(:file_url)
  end
end
