# -*- encoding : utf-8 -*-
require 'rabl'
Rabl.configure do |config|
  config.include_json_root = false
  config.include_child_root = false
  config.view_paths += %w(app/views/api/v1)
end
