Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'site#home'
  scope module: :api do
     scope module: :v1 do
       resources :company_groups , :defaults => { :format => 'json' } , :only => [:index]
       resources :companies , :defaults => { :format => 'json' }, :only => [:index, :show]
       resources :cinema , :defaults => { :format => 'json' }, :only => [:index, :show]
       resources :movies , :defaults => { :format => 'json' }, :only => [:index, :show]
       resources :cinema_baners , :defaults => { :format => 'json' }, :only => [:index]
       resources :forum_groups , :defaults => { :format => 'json' } , :only => [:index]
       resources :forum_themes , :defaults => { :format => 'json' }, :only => [:index, :show, :create]
       resources :forum_message , :defaults => { :format => 'json' }, :only => [:create ,:destroy]
       resources :media_assets, :defaults => { :format => 'json' }, :only => [:create, :destroy]
       resources :ad_group_links, :defaults => { :format => 'json' }, :only => [:create, :destroy]
       resources :company_group_links, :defaults => { :format => 'json' }, :only => [:create, :destroy]
       resources :recreation_group_links, :defaults => { :format => 'json' }, :only => [:create, :destroy]
       resources :ads , :defaults => { :format => 'json' }, :only => [ :index, :create, :destroy, :show ]
       resources :ad_groups , :defaults => { :format => 'json' }, :only => [:index]
       resources :ad_kinds , :defaults => { :format => 'json' }, :only => [:index]
       resources :recreations , :defaults => { :format => 'json' }, :only => [:index, :show]
       resources :recreation_groups , :defaults => { :format => 'json' }, :only => [:index]
       resources :events , :defaults => { :format => 'json' }, :only => [:index, :show]
       resources :event_groups , :defaults => { :format => 'json' } , :only => [:index]
       resources :atms , :defaults => { :format => 'json' }, :only => [:index]
       resources :atm_groups , :defaults => { :format => 'json' } , :only => [:index]
       resources :rates , :defaults => { :format => 'json' }, :only => [:create,:index]
       resources :reviews , :defaults => { :format => 'json' }, :only => [:index , :create ,:destroy]
       resources :dictionaries , :defaults => { :format => 'json' }, :only => [:index]
       resources :stocks , :defaults => { :format => 'json' }, :only => [:index]
       resources :transports , :defaults => { :format => 'json' }, :only => [:index ,:show]
       resources :emergencies , :defaults => { :format => 'json' }, :only => [:index ,:show]
       resources :news_items , :defaults => { :format => 'json' }, :only => [:index]
       resources :lifestyles , :defaults => { :format => 'json' }, :only => [:index, :show]
       resources :lifestyle_groups , :defaults => { :format => 'json' }, :only => [:index]
       resources :tickets , :defaults => { :format => 'json' }, :only => [:index]
       resources :exchanges , :defaults => { :format => 'json' }, :only => [:index]
       resources :resources , :defaults => { :format => 'json' }, :only => [:index]
       resources :albums , :defaults => { :format => 'json' }, :only => [:index ,:show]
       resources :restaurants , :defaults => { :format => 'json' }, :only => [:index,:show]
       resources :restaurants_categories , :defaults => { :format => 'json' }, :only => [:index]
       resources :foods_list , :defaults => { :format => 'json' }, :only => [:index]
       resources :goods , :defaults => { :format => 'json' }, :only => [:index]
       resources :purchase , :defaults => { :format => 'json' }, :only => [:create]
       resources :notification_subscribe , :defaults => { :format => 'json' }, :only => [:create]
       resources :main_dashboard , :defaults => { :format => 'json' }, :only => [:index]
       resources :baners_dashboard , :defaults => { :format => 'json' }, :only => [:index]
       resources :notifications, :defaults => { :format => 'json' }, :only => [:index]
       resources :address_to_delivery , :defaults => { :format => 'json' }, :only => [:index,:create,:destroy,:update]
       resource :users, :defaults => { :format => 'json' }, :only => [] ,:path => "/" do
         get :address
       end
       resource :users, :defaults => { :format => 'json' }, :only => [:update] do
         post :login
       end
       resources :filters , :defaults => { :format => 'json' } , :only => [:index]
       resources :search , :defaults => { :format => 'json' } , :only => [:index]

     end
   end
end
