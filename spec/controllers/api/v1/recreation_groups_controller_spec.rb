require 'rails_helper'

RSpec.describe Api::V1::RecreationGroupsController, type: :controller do
  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0" do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end
    it "GET recreation_groups" do
      recreation_groups = create_list :recreation_group, rand(1..5)
      get :index
      response_json = JSON.parse response.body
      response_json["data"].size.should eq(recreation_groups.size)
    end

    it "GET recreation_groups " do
      recreation_groups = create :recreation_group
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id name parent_id).include?(f).should eq(true)
        end
    end
  end

end
