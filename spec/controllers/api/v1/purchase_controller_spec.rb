require 'rails_helper'

RSpec.describe Api::V1::PurchaseController, type: :controller do

  before(:each) do
    request.headers["accept"] = 'application/json'
  end
  include ApiHelper
  include AuthHelper



  describe "POST create" do
    it "gives error_code 0", current_user: true do
      delivery = create :delivery, {:user => @current_user}
      food = create :food
      food_item = create :food_item
      purchase_good_links = [[:id => food_item.id ,:count => 3]]
      post :create, params:{:user_id => @current_user.id,:delivery_id =>delivery.id ,:restaurant_id => food.id, :purchase =>{:objects => purchase_good_links}}
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end
  end

end
