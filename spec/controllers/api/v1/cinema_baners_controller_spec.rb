require 'rails_helper'

RSpec.describe Api::V1::CinemaBanersController, type: :controller do

  before(:each) do
    request.headers["accept"] = 'application/json'
  end
  include ApiHelper
  include AuthHelper

  describe "GET index" do
    it "GET cinemas baners and error_code 0", current_user: true do
      cinema_baners = create :cinema_baner
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id name movie_id img).include?(f).should eq(true)
        end
    end
  end



end
