require 'rails_helper'

RSpec.describe Api::V1::LifestyleGroupsController, type: :controller do
  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0" do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end
    it "GET lifestyle_groups size" do
      lifestyle_groups = create_list :lifestyle_group, rand(1..5)
      get :index
      response_json = JSON.parse response.body
      response_json["data"].size.should eq(lifestyle_groups.size)
    end

    it "GET lifestyle_groups " do
      lifestyle_groups = create :lifestyle_group
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id name parent_id img).include?(f).should eq(true)
        end
    end
  end

end
