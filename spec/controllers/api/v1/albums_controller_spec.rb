require 'rails_helper'

RSpec.describe Api::V1::AlbumsController, type: :controller do\

  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0" do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end

    it "GET albums size " do
      albums = create_list :album , rand(1..5)
      get :index
      response_json = JSON.parse response.body
      response_json["data"].size.should eq(albums.size)
    end

    it "GET album columns " do
      albums = create :album
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id name image count review_count).include?(f).should eq(true)
        end
    end


  end

  describe "GET show" do

    it "gives error_code 0" do
      album = create :album
      get :show, params:{:id => album.id}
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end

    it "GET album " do
      album = create :album
      get :show, params:{:id => album.id}
      response_json = JSON.parse response.body
      response_json["data"]["id"].to_i.should eq(album.id)

    end

    it "GET album columns " do
      album = create :album
      get :show, params:{:id => album.id}
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].keys.each do |f|
          %w(id name phone address count review_count email social vk instagram facebook media albums_style).include?(f).should eq(true)
        end
    end

  end


end
