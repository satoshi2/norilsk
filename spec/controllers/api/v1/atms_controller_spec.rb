require 'rails_helper'

RSpec.describe Api::V1::AtmsController, type: :controller do

  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0" do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end

    it "GET atms " do

      atms = create_list :atm, rand(1..5)
      get :index
      response_json = JSON.parse response.body
      response_json["data"].size.should eq(atms.size)

    end
    it "GET atms " do
      atms = create :atm
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id address category atm_kind lat lon area_id).include?(f).should eq(true)
        end
    end


  end




end
