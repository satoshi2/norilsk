require 'rails_helper'

RSpec.describe Api::V1::EventsController, type: :controller do

  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0" do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end

    it "GET events " do

      events = create_list :event, rand(1..5)
      get :index
      response_json = JSON.parse response.body
      response_json["data"].size.should eq(events.size)

    end
    it "GET events " do
      events = create :event
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id name area_id begin_at finish_at description phone address counter).include?(f).should eq(true)
        end
    end
  end

  describe "GET show" do

    it "gives error_code 0" do
      event = create :event
      get :show, params:{:id => event.id}
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end

    it "GET event " do
      event = create :event
      get :show, params:{:id => event.id}
      response_json = JSON.parse response.body
      response_json["data"]["id"].to_i.should eq(event.id)

    end
  end

end
