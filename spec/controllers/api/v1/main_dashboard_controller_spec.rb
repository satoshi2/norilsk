require 'rails_helper'

RSpec.describe Api::V1::MainDashboardController, type: :controller do
  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0" do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end


    it "GET values and covers " do
      values = create :value
      covers = create :cover
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].keys.each do |f|
          %w( weather sunrise sunset temperature wind news cinema sales events albums currencies usd eur title subtitle image).include?(f).should eq(true)
        end
    end

  end

end
