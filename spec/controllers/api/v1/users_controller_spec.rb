require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do

  include ApiHelper
  include AuthHelper


  before(:each) do
    request.headers["accept"] = 'application/json'
  end


  describe "GET login" do
    it "require params" do
      post :login
      expect(response).to_not have_http_status(:success)
    end
  end

  describe "GET /address", current_user: true do
    it "gives house" do
      area = create :area
      street = create :street, {:area_id => area.id}
      house = create :house, {:street_id => street.id}
      get :address, params: {:street_id => street.id}
      response.should be_success
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first["id"].should eq(house.id)
    end
    it "gives street" do
      area = create :area
      street = create :street, {:area_id => area.id}
      house = create :house, {:street_id => street.id}
      get :address, params: {:area_id => area.id}
      response.should be_success
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first["id"].should eq(house.id)
    end
    it "gives area" do
      area = create :area
      get :address
      response.should be_success
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first["id"].should eq(area.id)
    end
    it "gives area" do
      area = create :area
      get :address
      response.should be_success
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id title ).include?(f).should eq(true)
        end
    end
  end


  describe "PATCH update", current_user: true do
    it "updates User firebase_token" do
      firebase_token = Faker::Crypto.md5
      patch :update, params: {:user => {:firebase_token => firebase_token}}
      response.should be_success
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"]["firebase_token"].should eq(firebase_token)
    end
    it "updates User house_id" do
      area = create :area
      street = create :street, {:area_id => area.id}
      house = create :house, {:street_id => street.id}
      patch :update, params: {:user => {:house_id => house.id}}
      response.should be_success
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"]["house_id"].should eq(house.id)
    end
  end
end
