require 'rails_helper'

RSpec.describe Api::V1::EmergenciesController, type: :controller do\

  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0" do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end
    it "GET emergencies" do
      emergencies = create_list :emergency, rand(1..5)
      get :index
      response_json = JSON.parse response.body
      response_json["data"].size.should eq(emergencies.size)
    end

    it "GET emergencies " do
      emergencies = create :emergency
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id title phone).include?(f).should eq(true)
        end
    end

  end


end
