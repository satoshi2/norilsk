require 'rails_helper'

RSpec.describe Api::V1::AddressToDeliveryController, type: :controller do


  before(:each) do
    request.headers["accept"] = 'application/json'
  end
  include ApiHelper
  include AuthHelper

  describe "GET index" do
    it "GET delivery and error_code 0", current_user: true do
      delivery = create :delivery, {:user => @current_user}
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id porch code floor apartment name phone area_name street_name house_name area_id street_id house_id).include?(f).should eq(true)
      end
    end
  end

  describe "PATCH update" do
    it "PATCH delivery and error_code 0", current_user: true do
      delivery = create :delivery, {:user => @current_user}
      name = Faker::Company.name
      get :update ,params:{:id => delivery.id , :user => {:name => name}}
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].keys.each do |f|
          %w(id porch code floor apartment name phone area_name street_name house_name area_id street_id house_id).include?(f).should eq(true)
      end
      response_json["data"]["name"].should eq(name)
    end
  end

  describe "POST create" do
    it "gives error_code 0 and other keys", current_user: true do
      house = create :house
      post :create, params:{user:{:house_id => house.id, :porch => rand(1.10), :code => "V231V3123", :floor => rand(1.10), :apartment => rand(1.10),:name => "Heroku"}}
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].keys.each do |f|
          %w( area_name street_name house_name area_id street_id house_id).include?(f).should eq(true)
      end
    end
  end

  describe "DELETE" do
    it "gives error_code 0", current_user: true do
      delivery = create :delivery
      post :destroy, params:{:id => delivery.id}
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end
  end

end
