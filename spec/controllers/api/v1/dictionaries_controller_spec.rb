require 'rails_helper'

RSpec.describe Api::V1::DictionariesController, type: :controller do

  before(:each) do
    request.headers["accept"] = 'application/json'
  end

    describe "GET index" do

      it "gives error_code 0" do
        get :index
        response_json = JSON.parse response.body
        response_json["error_code"].should eq(0)
      end

      it "GET dictionary " do
        dictionary = create_list :dictionary , rand(1..5)
        get :index
        response_json = JSON.parse response.body
        response_json["data"].size.should eq(dictionary.size)
      end
      it "GET dictionary " do
        dictionary = create :dictionary
        get :index
        response_json = JSON.parse response.body
        response_json["error_code"].should eq(0)
        response_json["data"].first.keys.each do |f|
            %w(rules).include?(f).should eq(true)
          end
      end

    end



end
