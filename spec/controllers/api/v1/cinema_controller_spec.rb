require 'rails_helper'

RSpec.describe Api::V1::CinemaController, type: :controller do

  before(:each) do
    request.headers["accept"] = 'application/json'
  end
  include ApiHelper
  include AuthHelper

  describe "GET index" do
    it "GET cinemas and error_code 0", current_user: true do
      cinema = create :cinema
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id name address rating).include?(f).should eq(true)
        end
    end
  end

    describe "GET show", current_user: true do

      it "GET cinema and error_code 0", current_user: true do
        cinema = create :cinema
        hall = create :hall, {:cinema_id => cinema.id}
        get :show, params: {:id => cinema.id}
        response_json = JSON.parse response.body
        response_json["error_code"].should eq(0)
        response_json["data"].keys.each do |f|
            %w(id name phone address url description email report_email rating sellout logo halls social work_time media services stocks).include?(f).should eq(true)
          end
      end

    end



end
