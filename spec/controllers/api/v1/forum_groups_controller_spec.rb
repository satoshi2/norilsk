require 'rails_helper'

RSpec.describe Api::V1::ForumGroupsController, type: :controller do
  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0" do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end
    it "GET forum_groups" do
      forum_groups = create_list :forum_group, rand(1..5)
      get :index
      response_json = JSON.parse response.body
      response_json["data"].size.should eq(forum_groups.size)
    end

    it "GET forum_groups keys " do
      forum_groups = create :forum_group
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id name themes_count).include?(f).should eq(true)
        end
    end

  end

end
