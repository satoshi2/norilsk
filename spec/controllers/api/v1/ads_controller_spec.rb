require 'rails_helper'

RSpec.describe Api::V1::AdsController, type: :controller do

  before(:each) do
    request.headers["accept"] = 'application/json'
  end
  include ApiHelper
  include AuthHelper

  describe "GET index" do
    it "GET ad and error_code 0", current_user: true do
      ad_group = create :ad_group
      ad = create :ad, {:ad_group_id => ad_group.id ,:user_id => @current_user.id}
      get :index, params: {:group_id => ad_group.id}
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id title state body price phone name count urls hide status published_until category ad_kind).include?(f).should eq(true)
        end
    end
  end

    describe "GET show", current_user: true do

      it "GET ad and error_code 0", current_user: true do
        ad_group = create :ad_group
        ad = create :ad, {:ad_group_id => ad_group.id ,:user_id => @current_user.id}
        get :show, params: {:id => ad.id}
        response_json = JSON.parse response.body
        response_json["error_code"].should eq(0)
        response_json["data"].keys.each do |f|
            %w(id title state body price phone name count urls hide status published_until category ad_kind sellout logo).include?(f).should eq(true)
          end
      end

      it "gives error_code 0" do
        ad_group = create :ad_group
        ad = create :ad, {:ad_group_id => ad_group.id,:user_id => @current_user.id}
        get :show, params:{:id => ad.id}
        response_json = JSON.parse response.body
        response_json["error_code"].should eq(0)
        response_json["data"]["id"].to_i.should eq(ad.id)
      end

    end

    describe "POST create" do

      it "gives error_code 0", current_user: true do
        ad_kind = create :ad_kind
        ad_group = create :ad_group
        post :create, params:{ad:{:title => "test", :hide => 1, :state => 1, :price => "1500",:ad_kind_id => ad_kind.id, :ad_group_id => ad_group.id,:body => "Test",:name => "Andrew", :phone => "928412231" ,:publishing_days => rand(1..5)}}
        response_json = JSON.parse response.body
        response_json["error_code"].should eq(0)
      end

    end

    describe "DELETE" do
      it "gives error_code 0", current_user: true do
        ad_group = create :ad_group
        ad = create :ad, {:ad_group_id => ad_group.id,:user_id => @current_user.id}
        post :destroy, params:{:id => ad.id}
        response_json = JSON.parse response.body
        response_json["error_code"].should eq(0)
      end




    end

end
