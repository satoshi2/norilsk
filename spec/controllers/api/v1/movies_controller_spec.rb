require 'rails_helper'

RSpec.describe Api::V1::MoviesController, type: :controller do

  before(:each) do
    request.headers["accept"] = 'application/json'
  end
  include ApiHelper
  include AuthHelper

  describe "GET index" do
    it "GET movies and error_code 0", current_user: true do
      movies = create :movie
      movie_sessions = create :movie_session, {:movie_id => movies.id}
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id name duration genres rating sessions_today logo review_count cinema_id hall_id premiere).include?(f).should eq(true)
        end
    end
  end

    describe "GET show", current_user: true do

      it "GET movie and error_code 0", current_user: true do
        movies = create :movie
        movie_sessions = :movie_sessions, {:movie_id => movies.id}
        get :show, params: {:id => movies.id}
        response_json = JSON.parse response.body
        response_json["error_code"].should eq(0)
        response_json["data"].keys.each do |f|
            %w(id name duration genres censorship description rating sellout logo cinema_count review_count premiere).include?(f).should eq(true)
          end
      end

    end



end
