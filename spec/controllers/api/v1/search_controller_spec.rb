require 'rails_helper'

RSpec.describe Api::V1::SearchController, type: :controller do
  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0" do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end
    it "GET search pattern" do
      company = create :company
      get :index, params:{:pattern => company.name }
      response_json = JSON.parse response.body
      response_json["data"].first["title"].should eq(company.name)
    end

    it "GET search pattern keys " do
      company = create :company
      get :index, params:{:pattern => company.name }
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id title id kind).include?(f).should eq(true)
        end
    end

  end

end
