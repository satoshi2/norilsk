require 'rails_helper'

RSpec.describe Api::V1::RatesController, type: :controller do

  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  include ApiHelper
  include AuthHelper
  include RatingHelper
  include ActiveJob::TestHelper


    describe "POST create" do
      it "require user" do
        post :create
        expect(response).to_not have_http_status(:success)
      end

      it "creates rating ", current_user: true do
        a = create :company
        post :create, params:{rate:{:rate => 5, :rateable_type => a.class ,:rateable_id =>   a.id}}
        response_json = JSON.parse response.body
        response_json["error_code"].should eq(0)
      end

      it "gives error_code 3", current_user: true do
        post :create, params:{rate:{:rate => 5, :rateable_type => "Company" ,:rateable_id => rand(1..5)}}
        response_json = JSON.parse response.body
        response_json["error_code"].should eq(3)
      end


      it "fetching rating value for rateable" do

        company = create :company

        rates = []
        perform_enqueued_jobs do
          rates = create_list :rate, rand(1..5), {:rateable => company }
        end
        rates_values = rates.map{ |a| a.rate }
        calculated_rating = rates_values.reduce(0, :+).to_f/rates_values.size

        company.reload

        company.rating.should eq(calculated_rating.round(2))

      end

    end


end
