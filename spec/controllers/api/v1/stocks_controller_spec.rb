require 'rails_helper'

RSpec.describe Api::V1::StocksController, type: :controller do

    before(:each) do
      request.headers["accept"] = 'application/json'
    end

    describe "GET index" do

      it "gives error_code 0" do
        get :index
        response_json = JSON.parse response.body
        response_json["error_code"].should eq(0)
      end

      it "GET stocks " do
        stocks = create_list :stock , rand(1..5)
        get :index
        response_json = JSON.parse response.body
        response_json["data"].size.should eq(stocks.size)
      end
      it "GET stocks " do
        stocks = create :stock
        get :index
        response_json = JSON.parse response.body
        response_json["error_code"].should eq(0)
        response_json["data"].first.keys.each do |f|
            %w(id title area_id preview begin_at finish_at view_count company_name company_id company_type body image_urls).include?(f).should eq(true)
          end
      end

    end

    # describe "GET show" do
    #
    #   it "gives error_code 0" do
    #     stock = create :stock
    #     get :show, params:{:id => stock.id}
    #     response_json = JSON.parse response.body
    #     response_json["error_code"].should eq(0)
    #   end
    #
    #   it "GET stock " do
    #     stock = create :stock
    #     get :show, params:{:id => stock.id}
    #     response_json = JSON.parse response.body
    #     response_json["data"]["id"].to_i.should eq(stock.id)
    #
    #   end
    #
    # end

end
