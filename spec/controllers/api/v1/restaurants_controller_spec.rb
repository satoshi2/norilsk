require 'rails_helper'

RSpec.describe Api::V1::RestaurantsController, type: :controller do

  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0" do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end

    it "GET restaurants " do
      foods = create_list :food , rand(1..5)
      get :index
      response_json = JSON.parse response.body
      response_json["data"].size.should eq(foods.size)
    end

    it "GET restaurants " do
      foods = create :food
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id name url area_id).include?(f).should eq(true)
        end
    end

  end

  describe "GET show" do


    it "GET restaurant " do
      food = create :food
      get :show, params:{:id => food.id}
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].keys.each do |f|
          %w(id name phone sellout address url logo categories description email email report_email rating count delivery categories social work_time media services staffs stocks vk facebook instagram ok).include?(f).should eq(true)
        end
    end

  end

end
