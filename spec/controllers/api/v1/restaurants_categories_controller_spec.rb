require 'rails_helper'

RSpec.describe Api::V1::RestaurantsCategoriesController, type: :controller do

  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0" do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end
    it "GET restaurants categories" do
      food_groups = create_list :food_group, rand(1..5)
      get :index
      response_json = JSON.parse response.body
      response_json["data"].size.should eq(food_groups.size)
    end

    it "GET restaurants categories " do
      food_groups = create :food_group
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id name parent_id ).include?(f).should eq(true)
        end
    end

  end
end
