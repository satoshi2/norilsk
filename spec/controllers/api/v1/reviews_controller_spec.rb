require 'rails_helper'

RSpec.describe Api::V1::ReviewsController, type: :controller do


  before(:each) do
    request.headers["accept"] = 'application/json'
  end
  include ApiHelper
  include AuthHelper


  describe "GET index" do
    it "GET ad and error_code 0", current_user: true do
      companies = create :company
      review = create :review, {:user_id => @current_user.id ,:reviewable_id => companies.id,:reviewable_type => companies.class}
      get :index, params:{:reviewable_id => companies.id,:reviewable_type => companies.class}
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
        %w(id title body created).include?(f).should eq(true)
      end
    end
  end

  describe "POST create"do
    it "gives error_code 0", current_user: true do
      companies = create :company
      post :create, params:{review:{:title => "test",:body => "test", :user_id => @current_user.id,:reviewable_id => companies.id,:reviewable_type => companies.class}}
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end
  end

  describe "DELETE" do
    it "gives error_code 0", current_user: true do
      companies = create :company
      review = create :review, {:user_id => @current_user.id ,:reviewable_id => companies.id,:reviewable_type => companies.class}
      post :destroy, params:{:id => review.id}
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end
  end
end
