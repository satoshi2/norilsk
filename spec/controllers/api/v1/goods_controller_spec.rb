require 'rails_helper'

RSpec.describe Api::V1::GoodsController, type: :controller do

  before(:each) do
    request.headers["accept"] = 'application/json'
  end


  describe "GET index" do

    it "GET goods" do
      food = create :food
      food_kind = create :food_kind, {:food => food}
      food_item = create :food_item, {:food_kind => food_kind}
      get :index, params: {:category_id => food_kind.id}
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      %w(id name urls price desc rate count weight structure).each do |key|
        response_json["data"].first.keys.include?(key).should eq(true)
      end
    end
  end

end
