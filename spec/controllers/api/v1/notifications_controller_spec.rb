require 'rails_helper'

RSpec.describe Api::V1::NotificationsController, type: :controller do


  include AuthHelper


  describe "GET index" do

    it "gives error_code 0 and json keys", current_user: true do
      notification = create :notification ,{:notificable => @current_user}
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      %w(id title body created notificable_id notificable_type).each do |key|
        response_json["data"].first.keys.include?(key).should eq(true)
      end
    end
  end

end
