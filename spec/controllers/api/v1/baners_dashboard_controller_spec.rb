require 'rails_helper'

RSpec.describe Api::V1::BanersDashboardController, type: :controller do
  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0" do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end

    it "GET ad_groups " do
      baner = create :baner
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id title body banerable_id banerable_type url).include?(f).should eq(true)
        end
    end

  end

end
