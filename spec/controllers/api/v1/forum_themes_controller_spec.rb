require 'rails_helper'

RSpec.describe Api::V1::ForumThemesController, type: :controller do

  before(:each) do
    request.headers["accept"] = 'application/json'
  end
  include ApiHelper
  include AuthHelper

  describe "GET index" do
    it "GET ad and error_code 0", current_user: true do
      forum_themes = create :forum
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id name body author messages_count created).include?(f).should eq(true)
        end
    end
  end

    describe "GET show", current_user: true do

      it "GET ad and error_code 0", current_user: true do
        forum_themes = create :forum
        forum_messages = create :forum_message , {:forum_id => forum_themes.id ,:user_id => @current_user.id}
        get :show, params: {:id => forum_themes.id}
        response_json = JSON.parse response.body
        response_json["error_code"].should eq(0)
        response_json["data"].keys.each do |f|
            %w(id name body forum_messages messages_count created ).include?(f).should eq(true)
          end
      end

    end

    describe "POST create" do

      it "gives error_code 0", current_user: true do
        forum_group = create :forum_group
        post :create, params:{forum_themes:{:name => "test",:body => "Test text", :author =>"Text name",:forum_group_id => forum_group.id}}
        response_json = JSON.parse response.body
        response_json["error_code"].should eq(0)
      end

    end



end
