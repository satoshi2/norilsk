require 'rails_helper'

RSpec.describe Api::V1::NewsItemsController, type: :controller do\

  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0" do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end
    it "GET news_items" do
      news_items = create_list :news_item, rand(1..5)
      get :index
      response_json = JSON.parse response.body
      response_json["data"].size.should eq(news_items.size)
    end

    it "GET news_items " do
      news_items = create :news_item
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id title source_url image_url created_at).include?(f).should eq(true)
        end
    end

  end


end
