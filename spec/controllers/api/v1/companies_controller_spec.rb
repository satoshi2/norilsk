require 'rails_helper'

RSpec.describe Api::V1::CompaniesController, type: :controller do

  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0" do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end

    it "GET companies " do
      companies = create_list :company , rand(1..5)
      get :index
      response_json = JSON.parse response.body
      response_json["data"].size.should eq(companies.size)
    end
    it "GET companies " do
      companies = create :company
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id name phone area_id address url description email rating social report_email staffs work_time media services vk facebook instagram stocks).include?(f).should eq(true)
        end
    end

  end

  describe "GET company" do

    it "GET company" do
      company = create :company
      get :show, params:{:id => company.id}
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"]["id"].to_i.should eq(company.id)
    end
  end

end
