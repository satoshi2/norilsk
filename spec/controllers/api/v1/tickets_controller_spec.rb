require 'rails_helper'

RSpec.describe Api::V1::TicketsController, type: :controller do\

  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0" do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end
    it "GET tickets " do
      tickets = create :ticket
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
          %w(id name source_url image_url ).include?(f).should eq(true)
        end
    end

  end

end
