require 'rails_helper'

RSpec.describe Api::V1::RecreationsController, type: :controller do
  before(:each) do
    request.headers["accept"] = 'application/json'
  end

  describe "GET index" do

    it "gives error_code 0" do
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
    end

    it "GET recreations " do
      recreations = create_list :recreation , rand(1..5)
      get :index
      response_json = JSON.parse response.body
      response_json["data"].size.should eq(recreations.size)
    end

    it "GET recreations " do
      recreations = create :recreation
      get :index
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"].first.keys.each do |f|
        %w(id name area_id phone address url description email rating social report_email staffs work_time media services vk facebook instagram).include?(f).should eq(true)
        end
    end
  end

  describe "GET show" do

    it "gives error_code 0" do
      recreation = create :recreation
      get :show, params:{:id => recreation.id}
      response_json = JSON.parse response.body
      response_json["error_code"].should eq(0)
      response_json["data"]["id"].to_i.should eq(recreation.id)

    end
  end

end
