FactoryGirl.define do
  factory :cinema_baner do
    name {Faker::Company.name}
    movie {create :movie}
  end
end
