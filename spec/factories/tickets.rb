FactoryGirl.define do
  factory :ticket do
    name  {Faker::Company.name}
    source_url {Faker::Internet.url}
  end
end
