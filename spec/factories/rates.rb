FactoryGirl.define do
  factory :rate do
    rate {rand(1..5)}
    rateable{create :company}
    user {create :user}
  end
end
