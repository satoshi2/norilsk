FactoryGirl.define do
  factory :food_item do
    name { Faker::Lorem.word }
    price {rand(5..10)}
    desc { Faker::Lorem.word }
    rate {rand(1..2)}
    count {rand(1..2)}
    structure { Faker::Lorem.word }
    food_kind {create :food_kind}
  end
end
