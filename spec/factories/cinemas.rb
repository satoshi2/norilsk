FactoryGirl.define do
  factory :cinema do
    name {Faker::Company.name}
    phone {Faker::Company.duns_number}
    address {Faker::Address.street_address}
    description {Faker::Demographic.race}
    email {Faker::Internet.email}
    report_email {Faker::Internet.email}
    url {Faker::Internet.email}
  end
end
