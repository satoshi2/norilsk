FactoryGirl.define do
  factory :baner do
    title {Faker::Company.name}
    body {Faker::Company.name}
    banerable {create :food}
  end
end
