FactoryGirl.define do
  factory :forum do
    name {Faker::Company.name}
    body {Faker::Company.name}
    author {Faker::Company.name}
    user {create :user}
  end
end
