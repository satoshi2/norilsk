FactoryGirl.define do
  factory :atm do
    address {Faker::Address.street_address}
    atm_kind {create :atm_kind}
  end
end
