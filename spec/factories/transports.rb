FactoryGirl.define do
  factory :transport do
    route_name {Faker::Internet.url}
  end
end
