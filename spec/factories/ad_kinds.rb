FactoryGirl.define do
  factory :ad_kind do
    name { Faker::Lorem.word }
  end
end
