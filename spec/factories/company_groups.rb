FactoryGirl.define do
  factory :company_group do
    name { Faker::Lorem.word }
    parent_id nil
  end
end
