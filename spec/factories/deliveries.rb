FactoryGirl.define do
  factory :delivery do
    user {create :user}
    house {create :house}
    porch {rand {1..10}}
    code "V220V1423"
    floor {rand {1..10}}
    apartment {rand {1..10}}
    name {Faker::Company.name}
  end
end
