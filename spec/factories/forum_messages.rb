FactoryGirl.define do
  factory :forum_message do
    author {Faker::Company.name}
    body {Faker::Company.name}
    user {create :user}
    forum {create :forum}
  end
end
