FactoryGirl.define do
  factory :food_kind do
    name { Faker::Lorem.word }
    food {create :food}
  end
end
