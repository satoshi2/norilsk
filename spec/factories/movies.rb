FactoryGirl.define do
  factory :movie do
    name {Faker::Company.name}
    description {Faker::Demographic.race}
    premiere { Faker::Time.between(DateTime.now - 1, DateTime.now) }
    duration { Faker::Lorem.word }
    genres { Faker::Lorem.word }
    censorship { Faker::Lorem.word }
    rating 1.5
    cinema {create :cinema}
    hall {create :hall}
  end
end
