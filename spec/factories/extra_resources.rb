FactoryGirl.define do
  factory :extra_resource do
    kind  {Faker::Company.name}
    source_url {Faker::Internet.url}
  end
end
