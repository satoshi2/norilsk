FactoryGirl.define do
  factory :review do
    user_id 1
    body { Faker::Lorem.word }
    reviewable_type "MyString"
    reviewable_id 1
  end
end
