FactoryGirl.define do
  factory :news_item do
    title "MyString"
    body "MyString"
    source_url "MyString"
  end
end
