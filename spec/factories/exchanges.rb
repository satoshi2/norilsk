FactoryGirl.define do
  factory :exchange do
    name  {Faker::Company.name}
    source_url {Faker::Internet.url}
  end
end
