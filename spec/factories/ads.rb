FactoryGirl.define do
  factory :ad do
    title {Faker::Company.name}
    state {rand(0..1)}
    hide {rand(0..1)}
    ad_group {create :ad_kind}
    body {Faker::Demographic.race}
    price {rand(100..500)}
    phone {Faker::Company.duns_number}
    name {Faker::Company.name}
    ad_kind{create :ad_kind}
    publishing_days {rand(1.10)}
    status "active"
  end
end
