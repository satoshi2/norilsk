FactoryGirl.define do
  factory :lifestyle_group do
    name  {Faker::Company.name}
    parent_id nil
  end
end
