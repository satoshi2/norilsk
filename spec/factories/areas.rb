FactoryGirl.define do
  factory :area do
    title { Faker::Lorem.word }
  end
end
