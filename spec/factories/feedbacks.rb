FactoryGirl.define do
  factory :feedback do
    user_id 1
    description "MyText"
    feedbackable_id 1
    feedbackable_type "MyString"
  end
end
