FactoryGirl.define do
  factory :recreation_group do
    name { Faker::Lorem.word }
    parent_id nil
  end
end
