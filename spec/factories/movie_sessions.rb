FactoryGirl.define do
  factory :movie_session do
    movie {create :movie}
    begin_at {DateTime.now + (2/24.0)}
    finish_at {DateTime.now + (4/24.0)}
  end
end
