FactoryGirl.define do
  factory :atm_group do
    name { Faker::Lorem.word }
    parent_id nil
  end
end
