FactoryGirl.define do
  factory :media_asset do
    url "MyString"
    mediable_id 1
    mediable_type "MyString"
    kind "MyString"
  end
end
