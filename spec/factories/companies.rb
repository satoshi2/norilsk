FactoryGirl.define do
  factory :company do
    name  {Faker::Company.name}
    phone  {Faker::Company.duns_number}
    address {Faker::Address.street_address}
    url {Faker::Internet.url}
    description {Faker::Internet.url}
    email {Faker::Internet.email}
    report_email {Faker::Internet.email}
    rating 0
    vk {Faker::Internet.url}
    instagram {Faker::Internet.url}
    facebook {Faker::Internet.url}
  end
end
