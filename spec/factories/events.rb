FactoryGirl.define do
  factory :event do
    name {Faker::Company.name}
    begin_at Faker::Time.between(DateTime.now - 1, DateTime.now)
    finish_at Faker::Time.between(2.days.ago, Date.today, :evening)
    description {Faker::Address.street_address}
    phone {Faker::Company.duns_number}
    address {Faker::Address.street_address}
    counter {rand(1.200)}
  end
end
