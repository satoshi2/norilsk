FactoryGirl.define do
  factory :event_group do
    name { Faker::Lorem.word }
    parent_id nil
  end
end
