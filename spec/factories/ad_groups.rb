FactoryGirl.define do
  factory :ad_group do
    name { Faker::Lorem.word }
    pay {["","false"]}
    parent_id nil
  end
end
