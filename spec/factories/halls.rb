FactoryGirl.define do
  factory :hall do
    name {Faker::Company.name}
    cinema {create :cinema}
  end
end
