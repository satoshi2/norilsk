FactoryGirl.define do
  factory :stock do
    begin_at "2017-06-08 12:30:30"
    finish_at "2017-06-08 12:30:30"
    title "MyString"
    view_count 1
    stockable {create :food}
    body {Faker::Demographic.race}
  end
end
