FactoryGirl.define do
  factory :user do
    phone_number { "+7#{Faker::Number.number(10)}" }
    api_token { Faker::Crypto.md5 }
  end
end
