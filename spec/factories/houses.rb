FactoryGirl.define do
  factory :house do
    title { Faker::Lorem.word }
    street {create :street}
  end
end
