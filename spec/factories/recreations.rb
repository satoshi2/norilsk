FactoryGirl.define do
  factory :recreation do
    name  {Faker::Company.name}
    phone  {Faker::Company.duns_number}
    address {Faker::Address.street_address}
    url {Faker::Internet.url}
    description {Faker::Demographic.race}
    email {Faker::Internet.email}
    rating {rand(0..5)}
    vk {Faker::Internet.url}
    instagram {Faker::Internet.url}
    facebook {Faker::Internet.url}
    report_email {Faker::Internet.url}
  end
end
