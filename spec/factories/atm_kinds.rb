FactoryGirl.define do
  factory :atm_kind do
    name { Faker::Lorem.word }
  end
end
