FactoryGirl.define do
  factory :cover do
    title {Faker::Company.name}
    body {Faker::Company.name}
    kind {Faker::Company.name}
  end
end
