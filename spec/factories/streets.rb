FactoryGirl.define do
  factory :street do
    title { Faker::Lorem.word }
    area {create :area}
  end
end
