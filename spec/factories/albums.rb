FactoryGirl.define do
  factory :album do
    name  {Faker::Company.name}
    phone  {Faker::Company.duns_number}
    address {Faker::Address.street_address}
    email {Faker::Internet.email}
    vk {Faker::Internet.url}
    instagram {Faker::Internet.url}
    facebook {Faker::Internet.url}
  end
end
